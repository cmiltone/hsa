<!doctype html><!-- Declaring document as HTML5 -->
<?php include 'backend/api.php'; ?> <!--  call api.php -->
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Home Service - App</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <!-- where the main application will be displayed -->
  <app-root></app-root>
</body>
</html>
