import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';

///////custom////
//modules
import { MainModule } from './main/main.module';
import { ServicesModule } from './services/services.module';

//routing
import { AppRouting } from './app.routing';
import { ServicesRouting } from './services/services.routing';

//components
import { MainComponent } from './main/main.component';
import { ServicesComponent } from './services/services.component';
import { AuthComponent } from './auth/auth.component';
import { AuthService } from './auth/auth.service';
import { ApiService } from './api.service';
import { MessageService } from './messaging/message.service';
import { ProfileResolverService } from './services/profile-resolver.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { MessagingComponent } from './messaging/messaging.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { AdminGuardService } from './auth/admin-guard.service';
import { ImageService } from './image.service';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ServicesComponent,
    AuthComponent,
    MessagingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRouting,
    ServicesRouting,
    MainModule,
    ServicesModule,
    DashboardModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},//to be able to use external links
    AuthService, ApiService, MessageService, ProfileResolverService, AuthGuardService, AdminGuardService, ImageService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
