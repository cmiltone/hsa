import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './main.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';

const mainRoutes: Routes = [
	{
		path: '', component: MainComponent,
		children:
		[
			{ path: '', component: HomeComponent },
			{ path: 'about', component: AboutComponent }
		]
	},	
] 
export const MainRouting: ModuleWithProviders = RouterModule.forChild(mainRoutes);
