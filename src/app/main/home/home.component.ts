import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	title = "Popular Services";
	currentIndex: number = 0;

	slides = [
		{name: 'Babbysitting', image: 'babbysitting.jpeg'},
		{name: 'Cleaning', image: 'cleaning.jpeg'},
		{name: 'Interior Decor', image: 'decor.jpeg'},
		{name: 'Renovation', image: 'renovation.jpeg'},
		{name: 'Shopping', image: 'shopping.jpeg'},
	]

	constructor(private router: Router) { }

	ngOnInit()
	{
		let _this = this;
		setInterval(function(){
		  	_this.nextSlide();
	  	},5000);
	}

	gotoServices()
	{
		this.router.navigate(['services']);
	}

	nextSlide()
	{
		this.currentIndex++;
		if(this.currentIndex == this.slides.length)
		{
			this.currentIndex = 0;
		}
	}

	prevSlide()
	{
		this.currentIndex--;
		if(this.currentIndex < 0)
		{
			this.currentIndex = this.slides.length - 1;
		}

	}

}