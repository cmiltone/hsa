import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRouting } from './main.routing';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  imports: [
    CommonModule,
    MainRouting/**/
  ],
  declarations: [HomeComponent, AboutComponent]
})
export class MainModule { }
