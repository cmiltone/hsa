import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth/auth.component';

const appRoutes: Routes = [
	{
		path: 'auth', component: AuthComponent
	}
] 
export const AppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
