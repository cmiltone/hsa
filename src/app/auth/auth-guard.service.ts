import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';


@Injectable()
export class AuthGuardService implements CanActivate{

	constructor(public auth: AuthService, private router: Router ) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean
	{
		let url: string = state.url;

		return this.checkLogin(url);
	}

	checkLogin(url: string): boolean
	{
		if(this.auth.authToken)
		{
			if(this.auth.authToken.l) { return true;}
			
			this.auth.setRedirect(url);
			this.router.navigate(['/auth']);
		}

		this.auth.setRedirect(url);

		this.router.navigate(['/auth']);

		return false;
	}

}
