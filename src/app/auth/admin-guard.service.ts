import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';
import { MessageService } from '../messaging/message.service';


@Injectable()
export class AdminGuardService implements CanActivate{

	constructor(
		public auth: AuthService,
		private router: Router,
		private msg: MessageService
	) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean
	{
		let url: string = state.url;

		return this.checkRole(url);
	}

	checkRole(url: string): boolean
	{
		if(this.auth.authToken)
		{
			if(this.auth.authToken.l && this.auth.authToken.role === 'ADMIN') { return true;}
			
			else this.msg.addMessage(`Access to '${url}' Denied for Current User Type`);
		}

		this.auth.setRedirect(url);

		this.router.navigate(['/auth']);

		return false;
	}

}
