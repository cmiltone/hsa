import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { MessageService } from '../messaging/message.service';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit
{
	reg: boolean = false;
	lf : FormGroup = new FormGroup({
		username: new FormControl('',Validators.required),
		password: new FormControl('',Validators.required),
	});

	rf : FormGroup = new FormGroup({
		fullname: new FormControl('',Validators.required),
		contact: new FormControl('',Validators.required),
		address: new FormControl('',Validators.required),
		username: new FormControl('',Validators.required),
		password: new FormControl('',Validators.required),
	});

	constructor(
		private auth: AuthService,
		private messageService: MessageService,
		private router: Router
	) { }

	ngOnInit()
	{
		//first check if is logged in
		this.auth.getAuthToken().subscribe(token=>{
			if(token.l as boolean == true)
			{
				//this.messageService.addMessage('Already Logged in. Redirecting...');
				this.router.navigate([this.auth.redirectUrl]);
			}
		});
	}

	login()
	{
		let user = this.lf.value;
		user.status = 'ACTIVE';
		this.auth.loginUser(user);
	}

	register()
	{
		let user = this.rf.value;
		console.log(user)
		let msg = ['Registration','Registration request sent'];
		this.auth.registerUser(user,msg);
	}

}
