import { Injectable } from '@angular/core';
import { Observable } 	from 'rxjs/Observable';
import { Router } from '@angular/router';

import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';

import { User } from '../models/user';
import { APIRequest, APIResponse } from '../models/interfaces';

import { ApiService } from '../api.service';
import { MessageService } from '../messaging/message.service';/**/

export interface AuthToken { uname:string, uid: number, l: boolean, role: string}

@Injectable()
export class AuthService
{
	public redirectUrl: string = '/';
	public authError: string = '';
	public authToken: AuthToken = null;

	constructor(
		private api: ApiService,
		private router: Router,
		private messageService: MessageService
	) { }

	public registerUser(user: User, msg: string[]): void
	{
		let payload: APIRequest = {
			params: { a: 'auth', target: "reg", table: "user", data: user },
			request_msg: { req_desc: msg[0], success_msg: msg[1] },
		};
		this.api.sendRequest(payload).subscribe(response=>{
			if(response.data.length)
				this.messageService.addMessage('Registration Successfull. Please Login');
			else if(response.error)
			{
				let e = JSON.stringify(response.error);
				this.messageService.addErrorMessage(e);
			}
		});

	}

	public loginUser(user: User)
	{
		// attempt login
		let payload: APIRequest = {
			params: {a: 'auth', target: "login", table: "user", filter: user },
			request_msg: {req_desc: "Login ", success_msg: "Login request Sent"},
		};
		this.api.sendRequest(payload).subscribe(response=>{
			if(response.error)
			{
				let e = JSON.stringify(response.error);
				this.messageService.addErrorMessage(e);;
			}
			else if(response.data.auth_token.l)
			{
				this.authToken = response.data.auth_token;				
				this.router.navigate([this.redirectUrl]);
				setTimeout(()=>{
					this.messageService.clear();
				},5000);
			}
		});			
	}

	public getAuthToken(): Observable<AuthToken>
	{
		let payload: APIRequest = {
			params: {a: "auth", target: "get", table: null },
			request_msg: { req_desc: 'A request ', success_msg: ""},
		};
		return this.api.sendRequest(payload).map(response=>{ return response.data.auth_token; });

	}

	public setRedirect(url): void
	{
		this.redirectUrl = url;
	}
}