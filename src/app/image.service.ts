import { Injectable } from '@angular/core';;
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } 	from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { APIRequest, APIResponse } from './models/interfaces';
import { mockResponse } from './models/mocks';

import { MessageService } from './messaging/message.service';

const options = { 
	headers: new HttpHeaders(/*{
		'Content-Type': 'multipart/form-data',
		'Accept': 'application/json',
		'Authorization':''
	}*/) 
};

@Injectable()
export class ImageService
{
	private apiURL: string = 'backend/api.php';
	
	constructor(
		private http: HttpClient,
		private messageService: MessageService
	) { }

	public sendFile(image: FormData): Observable<APIResponse>
	{
		// dummy result:- in case of http failure let the app continue
		let result: APIResponse = mockResponse;
		result.error = 'Could not get  a valid response from the Server: Network and/or Server Error!';

		//first check that data is ok

		//send the request and return response 
		return this.http.post<APIResponse>(this.apiURL, image, options).pipe(
			tap((response_payload)=> { this.log('Image Upload Request Sent'); }),
			catchError(this.errorHandler<APIResponse>('Image Upload Failed', result))
		);
	}

	/**
	* Handle failed operation.
	* Let app continue
	* @param operation - name of operation that failed
	* @param result - optional value to return as observable result
	*/

	private errorHandler<T>(operation = 'operation', result?: T)
	{
		return (error: any): Observable<T> =>{
			//TODO: send error to remote logging infrustructure
			console.log(error);

			//TODO: make error user friendly
			let error_text: string = 'Could not get  a valid response from the Server: Network and/or Server Error!';
			if(error.status)
			{
				 error_text = error.status == 404 ? 'Could Not Connect to Server' : error_text;
			}
			
			this.logError(`${operation} failed: ${error_text}`);

			//let app keep running; return empty result
			return of(result as T);

		}
	}

	private log(message: string)
	{
		this.messageService.addMessage(message);
	}

	private logError(e: string)
	{
		this.messageService.addErrorMessage(e);
	}

	public prepData(d)
	{
		// TODO: Validate input data:- check for SQL injection and other maliciuos data
		return JSON.parse(JSON.stringify(d));// remove undefined items
	}
}