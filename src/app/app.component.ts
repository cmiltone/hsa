import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent
{
	title = 'Home Service System';

	constructor(public auth: AuthService )
	{
		this.auth.getAuthToken().subscribe(auth=>{
			this.auth.authToken = auth;
		});
	}
}
