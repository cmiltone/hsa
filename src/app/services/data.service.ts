import { Injectable } from '@angular/core';

import { Observable } 	from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { APIResponse, APIRequest} from '../models/interfaces';
import { MessageService } from '../messaging/message.service';
import { ApiService } from '../api.service';


@Injectable()
export class DataService {

	constructor(private api: ApiService, private message: MessageService) { }

	public getRecords(table:string,filter:any,msg?:string[]): Observable<any[]>
	{
		msg = msg ? msg : ['Fetching Record(s)','Fetching Record(s) Failed'];
		let request: APIRequest = {
			params:{a:'query',target:'get',table:table,filter:filter},
			request_msg:{
				req_desc:msg[0],
				success_msg:'',
				error_msg:msg[1]
			}
		};
		return this.api.sendRequest(request).map(response=>{
			if(response.error)
				this.message.addErrorMessage(JSON.stringify(response.error));
			return response.data ? response.data : [];

		}); 
	}

	public addRecord(table:string,record:any,msg?:string[]): Observable<number>
	{
		msg = msg ? msg : ['Saving','Saving Record Failed'];
		let request: APIRequest = {
			params:{a:'query',target:'set',table:table,data:record},
			request_msg:{
				req_desc:msg[0],
				success_msg:'Save request sent',
				error_msg:msg[1]
			}
		};
		return this.api.sendRequest(request).map(response=>{
			if(response.error)
				this.message.addErrorMessage(JSON.stringify(response.error));
			return response.data ? response.data : 0 as number;

		}); 
	}

	public deleteRecord(table:string,filter:any,msg?:string[]): Observable<number>
	{
		msg = msg ? msg : ['Deleting Reocrd','Deleting Record Failed'];
		let request: APIRequest = {
			params:{a:'query',target:'delete',table:table,filter:filter},
			request_msg:{
				req_desc:msg[0],
				success_msg:'Delete request sent',
				error_msg:msg[1]
			}
		};
		return this.api.sendRequest(request).map(response=>{
			if(response.error)
				this.message.addErrorMessage(JSON.stringify(response.error));
			return response.data ? response.data : 0 as number;

		}); 
	}

	public updateRecord(table:string,filter:any,data:any,msg?:string[]): Observable<number>
	{
		msg = msg ? msg : ['Updating Record','Updating record Failed'];
		let request: APIRequest = {
			params:{a:'query',target:'update',table:table,filter:filter, data:data},
			request_msg:{
				req_desc:msg[0],
				success_msg:'Update request sent',
				error_msg:msg[1]
			}
		};
		return this.api.sendRequest(request).map(response=>{
			if(response.error)
				this.message.addErrorMessage(JSON.stringify(response.error));
			return response.data ? response.data : 0 as number;

		});  
	}

	public printRecords(params: {target:string,table:string,filter:any,msg:string[]}): Observable<string>
	{
		let request: APIRequest = {
			params:{a:'print',target:params.target,table:params.table,filter:params.filter, data:null},
			request_msg:{
				req_desc:params.msg[0],
				success_msg:'Print request sent',
				error_msg:params.msg[1]
			}
		};
		return this.api.sendRequest(request).map(response=>{
			if(response.error)
				this.message.addErrorMessage(JSON.stringify(response.error));
			return response.data ? response.data : '' as string;
		});
	}
}
