import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServicesComponent } from './services.component';
import { ServiceListComponent } from './service-list/service-list.component';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { ServiceResolverService } from './service-resolver.service';

const servicesRoutes: Routes = [
	{
		path: 'services', component: ServicesComponent,
		children:
		[
			{ path: '', component: ServiceListComponent },
			{ 
				path: ':service_id', component: ServiceDetailComponent,
				resolve: { service: ServiceResolverService }
			},
		]
	},	
] 
export const ServicesRouting: ModuleWithProviders = RouterModule.forChild(servicesRoutes);
