import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Service } from '../../models/service';
import { User } from '../../models/user';
import { Provider } from '../../models/provider';
import { DataService } from '../data.service';
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../messaging/message.service';

@Component({
	selector: 'app-service-list',
	templateUrl: './service-list.component.html',
	styleUrls: ['./service-list.component.css']
})
export class ServiceListComponent implements OnInit
{
	public services: Service[] = [];
	public services_file: string;
	public selected_id: number = null;

	public title = "Services";

	constructor(
		private data: DataService,
		private router: Router,
		public auth: AuthService,
		private msg: MessageService,
	) { }

	ngOnInit()
	{
		this.data.getRecords('service',null,['','Fetching Services Failed']).subscribe(services=>{
			if(services)
			{
				services.forEach((service)=>{
					this.services.push(new Service(service));
				});console.log(services);
			}
		});

	}

	request(service)
	{
		if(this.auth.authToken)
		{
			var _do = false;
			this.auth.authToken.role != 'PROVIDER' ? alert('Please Logout of Admin Account to continue') : _do = true;
			if(_do)
			{
				this.saveProvider(this.auth.authToken.uid,service);
			}

		}
	}

	saveProvider(id,service)
	{
		let msg = ['Fetching Your Details','Fetching User details Failed'];
		this.data.getRecords('user',{id:id},msg).subscribe(users=>{
			if(users)
			{
				let user = { fullname: users[0].fullname, contact: users[0].contact, address:users[0].address, id:users[0].id}
				let provider = {
					user_id: id,
					service_id: service.id,
					provider: JSON.stringify(user),
					cost: service.service_fee
				};
				console.log('saving provider->',provider);
				msg = ['Saving Request Sent','Request Failed'];
				this.data.addRecord('service_provider',provider,msg).subscribe(response=>{
					console.log('save respone->',response);
					if(response)
					{
						this.msg.addMessage('Your request Approved.');
					}
					else 
					{
						this.msg.addMessage('Could not Approve your Request at this time. Please Try later.');	
					}
				});
			}
		})
	}

	printServices(param:any)
	{
		let filter:any = param === 'all' ? null : { id: param};
		this.selected_id = param === 'all' ? null : param;
		let params = {
			msg: ['Printing Service(s)','Printing Service(s) Failed'],
			target: 'services',
			table: 'service',
			filter: filter
		}
		this.data.printRecords(params).subscribe(filename=>{
			if(filename.length) { this.services_file = filename; }
		})
	}

}