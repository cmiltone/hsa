import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

import { Observable } 	from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { User } from '../models/user';

import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';

@Injectable()
export class ProfileResolverService implements Resolve<User>
{
	public redirectTo: string = '/auth';
	constructor(
		private data: DataService,
		private router: Router,
		private auth: AuthService
	) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User>
	{
		let filter = {id: +this.auth.authToken.uid};
		let msg = ['','Fetching account Details failed'];
		return this.data.getRecords('user',filter,msg).map(profiles=>{
			if(profiles)
			{
				return profiles[0];
			}
			else
			{
				this.router.navigate([this.redirectTo]);
				return null;
			}
		});
	}


}