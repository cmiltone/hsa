import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//routing
import { ServicesRouting } from './services.routing';

import { ServiceListComponent } from './service-list/service-list.component';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { DataService } from './data.service';
import { ServiceResolverService } from './service-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ServicesRouting
  ],
  declarations: [ServiceListComponent, ServiceDetailComponent],
  providers: [DataService, ServiceResolverService]
})
export class ServicesModule { }
