import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, NgForm, NgModel } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Service } from '../../models/service';
import { Booking } from '../../models/booking';
import { Provider } from '../../models/provider';
import { User } from '../../models/user';

import { DataService } from '../data.service';
import { MessageService } from '../../messaging/message.service';

@Component({
	selector: 'app-service-detail',
	templateUrl: './service-detail.component.html',
	styleUrls: ['./service-detail.component.css']
})
export class ServiceDetailComponent implements OnInit
{
	providers: Provider[] = [];
	title: string;
	order_id: number;
	order_link:string;
	order: Booking;
	requesting: boolean = false;

	public f = new FormGroup({
		client: new FormGroup({
			fullname: new FormControl('',Validators.required),
			contact: new FormControl('',[Validators.required,Validators.email]),
			address: new FormControl('',Validators.required),
		}),
		product: new FormControl('',Validators.required),
		provider: new FormControl('',Validators.required),
	});

	bookError: any;

	constructor(
		private route: ActivatedRoute,
		private data: DataService,
		private msg: MessageService,
	) { }

	ngOnInit()
	{ 
		this.route.data.subscribe((data: { service: Service})=>{
			let client = {fullname:null,contact:null,address:null};
			this.order = new Booking({
				client:JSON.stringify(client),
				client_id:null,
				product:JSON.stringify(data.service),
				provider_id: null,
				provider:null,
				status: 'NEW'
			});
			this.getProviders(data.service.id);
			this.title = `'${this.order.product.label}' Service Details`;
		});
	}

	book()
	{
		console.log(this.f.value);
		if(confirm('Request Service'))
		{
			let booking: any = this.f.value;
			this.requesting = true;
			booking.product = JSON.stringify(booking.product);
			booking.client = JSON.stringify(booking.client);
			booking.provider = JSON.stringify(booking.provider);
			booking.provider_id = this.order.provider.user_id;console.log('booking->',booking);
			let msg = ['New Order Request Sent','New Order Request Failed'];
			this.data.addRecord('client_order',booking,msg).subscribe(response=>{
				if(response)
				{
					this.msg.addMessage('Your Order was Saved Successfully. Please print the details. Our team will Contact you soon');
					this.order_id = response;
				}
				this.order = new Booking(booking);
				this.requesting = false;
			});
		}
	}

	getProviders(id: number)
	{
		let msg = ['',''];
		let filter = { service_id: id};
		this.data.getRecords('service_provider',filter,msg).subscribe(providers=>{
			providers.forEach((provider)=>{
				this.providers.push(new Provider(provider));
			});
		})
	}

	printOrder(id)
	{
		let params = {
			msg: ['Printing Order','Printing Order Failed'],
			target: 'orders',
			table: 'client_order',
			filter: {id:id}
		}
		this.data.printRecords(params).subscribe(filename=>{
			if(filename.length) { this.order_link = filename; }
		});
	}

}
