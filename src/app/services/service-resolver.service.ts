import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

import { Observable } 	from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { Service } from '../models/service';

@Injectable()
export class ServiceResolverService implements Resolve<Service>
{
	public redirectTo: string = '/services';
	constructor(private data: DataService, private router: Router) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Service>
	{
		let id:number = +route.paramMap.get('service_id');console.log(id);
		let filter = {id:id};
		//state.url ? this.redirectTo = state.url : null;
		console.log('state.url->',state.url);
		
		return this.data.getRecords('service',filter,['','']).map(services=>{
			if(services)
			{
				let s = new Service(services[0]);
				console.log('resolving service',s);
				return s;
			}
			else
			{
				this.router.navigate([this.redirectTo]);
				return null;
			}
		});
	}


}