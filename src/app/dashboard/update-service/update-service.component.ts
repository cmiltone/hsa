import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, NgForm, NgModel } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Service } from '../../models/service';
import { User } from '../../models/user';
import { APIRequest } from '../../models/interfaces';

import { DataService } from '../../services/data.service';
import { MessageService } from '../../messaging/message.service';
import { ImageService } from '../../image.service';

@Component({
	selector: 'app-update-service',
	templateUrl: './update-service.component.html',
	styleUrls: ['./update-service.component.css']
})
export class UpdateServiceComponent implements OnInit
{
	fAction: string;
	service: Service;

	public f = new FormGroup({
		label: new FormControl('',Validators.required),
		service_fee: new FormControl('',Validators.required),
		description: new FormControl('',Validators.required),
		image: new FormControl('',Validators.required),
	});

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private data: DataService,
		private msg: MessageService,
		private imageService: ImageService
	) { }

	ngOnInit()
	{
		console.log('snapshot.url->',this.route.snapshot.url);
		if(this.route.snapshot.url[0].path == 'new')
		{
			this.service = new Service({
				label:'New',
				description:null,
				service_fee:0.00,
				image: 'placeholder.png'
			});
			//this.service.image = 'placeholder.png';
		}else
		{
			this.getService();
		}
		this.service.image = this.service.image.split('/',)[3];
		
	}

	getService()
	{
		this.route.data.subscribe((data: { service: Service})=>{
			this.service = data.service;
		});		
	}

	uploadImage(images: File[])
	{
		console.log(images);
		if(images.length)
		{
			let image = images[0];
			// upload image code
			let imageData = new FormData();
			imageData.append('image',image,image.name);
			this.imageService.sendFile(imageData).subscribe(response=>{
				console.log(response);
				this.service.image = image.name;
			});
		}
	}

	submit()
	{
		console.log('saving->',this.f.value);
		switch (this.fAction) {
			case "add":
			{
				this.save(this.f.value);
			}break;
			case "update":
			{
				this.update(this.f.value);
			}break;
			case "delete":
			{
				this.deleteService(this.f.value);
			}break;
			
			default:
				// code...
				break;
		}
	}

	save(service)
	{
		this.data.addRecord('service',service).subscribe(response=>{
			if(response)
			{
				this.msg.addMessage('Service Saved successfully');
				this.router.navigate(['dashboard','update',response]);
			}
		});
	}

	update(service)
	{
		let filter = { id: this.service.id};
		this.data.updateRecord('service',filter,service).subscribe(response=>{
			if(response == 1)
			{
				this.msg.addMessage('Service Updated successfully');
				this.router.navigate(['dashboard','update',this.service.id]);
			}
		});
	}
	deleteService(service)
	{
		let filter = { id: this.service.id};
		this.data.deleteRecord('service',filter).subscribe(response=>{
			if(response == 1)
			{
				this.msg.addMessage('Service deleted successfully');
				this.router.navigate(['services']);
			}
		});
	}

}
