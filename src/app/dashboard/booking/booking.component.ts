import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, NgForm, NgModel } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Booking } from '../../models/booking';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../messaging/message.service';

@Component({
	selector: 'app-booking',
	templateUrl: './booking.component.html',
	styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit
{
	title: string = '';
	orders: Booking[];
	selected_id: number = null;
	printed_id: number = null;
	orders_link: string;

	bf: FormGroup;

	constructor(
		private route: ActivatedRoute,
		private data: DataService,
		private msg: MessageService,
		public auth: AuthService,
	) { }

	ngOnInit()
	{
		this.bf = new FormGroup({
			status: new FormControl('',Validators.required),
		});
		this.title = `Orders`;
		this.route.data.subscribe((data: { bookings: Booking[]})=>{
			this.orders = data.bookings;
		});
	}

	update()
	{
		let filter = {id:this.selected_id};
		let record = this.bf.value;
		this.data.updateRecord('client_order',filter,record).subscribe(response=>{
			if(response == 1)
			{
				this.msg.addMessage('Order Updated Successfully');
			}
		});
	}

	printOrders(param:any)
	{
		let filter:any = this.auth.authToken.role === 'ADMIN' ? null : { provider_id:this.auth.authToken.uid };
		param === 'all' ? null : filter = { id: param};
		this.printed_id = param === 'all' ? null : param;
		let params = {
			msg: ['Printing Order(s)','Printing Order(s) Failed'],
			target: 'orders',
			table: 'client_order',
			filter: filter
		}
		this.data.printRecords(params).subscribe(filename=>{
			if(filename.length){this.orders_link = filename;}
		})
	}

}
