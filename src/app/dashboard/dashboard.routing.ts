import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { AccountComponent } from './account/account.component';
import { BookingComponent } from './booking/booking.component';
import { BookingResolverService } from './booking-resolver.service';
import { ServiceDetailComponent } from '../services/service-detail/service-detail.component';
import { UpdateServiceComponent } from './update-service/update-service.component';
import { ProviderComponent } from './provider/provider.component';
import { ServiceResolverService } from '../services/service-resolver.service';
import { ProfileResolverService } from '../services/profile-resolver.service';
import { AuthGuardService } from '../auth/auth-guard.service';
import { AdminGuardService } from '../auth/admin-guard.service';

const dashboardRoutes: Routes = [
	/*{ path: '**', redirectTo: 'dashboard' },*/
	{
		path: 'dashboard', component: DashboardComponent,
		canActivate: [AuthGuardService],
		children:
		[
			{
				path: '', redirectTo: 'account', pathMatch: 'full'
			},
			{
				path: 'orders', component: BookingComponent,
				resolve: { bookings: BookingResolverService },
				canActivate: [AdminGuardService]
			},
			{
				path: 'users', component: ProviderComponent,
				canActivate: [AdminGuardService]
			},
			{
				path: 'orders/:provider_id', component: BookingComponent,
				resolve: { bookings: BookingResolverService }
			},
			{
				path: 'new', component: UpdateServiceComponent,
				canActivate: [AdminGuardService]
			},
			{
				path: 'update/:service_id', component: UpdateServiceComponent,
				resolve: { service: ServiceResolverService }
			},
			{
				path: 'account', component: AccountComponent,
				resolve: { profile: ProfileResolverService }
			}
		]
	},	
] 
export const DashboardRouting: ModuleWithProviders = RouterModule.forChild(dashboardRoutes);
