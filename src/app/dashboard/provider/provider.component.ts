import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, NgForm, NgModel } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { User } from '../../models/user';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../messaging/message.service';

@Component({
	selector: 'app-provider',
	templateUrl: './provider.component.html',
	styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit
{
	title: string = '';
	users: User[] = [];
	selected_id: number = null;
	printed_id: number = null;
	users_link: string;

	uf: FormGroup = new FormGroup({
		status: new FormControl('',Validators.required),
		role: new FormControl('',Validators.required),
	});

	constructor(
		private route: ActivatedRoute,
		private data: DataService,
		private msg: MessageService,
		public auth: AuthService,
	) { }

	ngOnInit()
	{
		this.title = `Manage User Accounts`;
		let filter = null;
		this.data.getRecords('user',filter).subscribe(users=>{
			if(users)
			{
				this.users = users;
			}
		})
	}

	update()
	{
		let filter = {id:this.selected_id};
		let record = this.uf.value;
		this.data.updateRecord('user',filter,record).subscribe(response=>{
			if(response == 1)
			{
				this.msg.addMessage('User Account Updated Successfully');
			}
		});
	}

	printProviders(param:any)
	{
		let filter:any = param === 'all' ? null : { user_id: param};
		this.printed_id = param === 'all' ? null : param;
		let params = {
			msg: ["Printing Provider(s)' Details","Printing Provider(s)' Details Failed"],
			target: 'providers',
			table: 'service_provider',
			filter: filter
		}
		this.data.printRecords(params).subscribe(filename=>{
			if(filename.length) { this.users_link = filename;}
		})
	}

}
