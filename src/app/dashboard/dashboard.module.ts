import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard.component';
import { UpdateServiceComponent } from './update-service/update-service.component';
import { DashboardRouting } from './dashboard.routing';
import { AccountComponent } from './account/account.component';
import { BookingComponent } from './booking/booking.component';
import { BookingResolverService } from './booking-resolver.service';
import { ProviderComponent } from './provider/provider.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardRouting
  ],
  declarations: [DashboardComponent, UpdateServiceComponent, AccountComponent, BookingComponent, ProviderComponent],
  providers: [BookingResolverService]
})
export class DashboardModule { }
