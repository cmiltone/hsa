import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, NgModel, NgForm, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user';

import { DataService } from '../../services/data.service';
import { MessageService } from '../../messaging/message.service';

function passwordValid(g:FormGroup)
{

	return g.get('password').value === g.get('pass').value ? null : {'incorrect': true};
}

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit
{
	title: string;

	profile: User;

	pf: FormGroup = new FormGroup({
		fullname: new FormControl('',Validators.required),
		contact: new FormControl('',Validators.required),
		address: new FormControl('',Validators.required),
		username: new FormControl('',Validators.required),
		pass: new FormControl(),
		password: new FormControl('',Validators.required),
		npassword: new FormControl(),
	},passwordValid);


	constructor(
		private data: DataService,
		private route: ActivatedRoute,
		private msg: MessageService
	) { }

	ngOnInit()
	{
		this.route.data.subscribe((data: { profile: User})=>{
			this.profile = data.profile;
			this.pf.patchValue({pass:this.profile.password});
			this.title = `${this.profile.fullname}'s Account Details`;
		});
		
	}

	delete()
	{
		let filter = { id: this.profile.id};
		this.profile.id = undefined;
		this.profile.status = 'INACTIVE';
		console.log('updating->',this.profile);
		this.data.updateRecord('user',filter,this.profile).subscribe(response=>{ 
			if(response == 1)
			{
				this.msg.addMessage('Deactivated Successfully.');
			}
			this.profile.id = filter.id;
		});

	}

	update()
	{
		let filter = { id: this.profile.id};
		this.profile.id = undefined;
		this.pf.value.npassword ? this.profile.password = this.pf.value.npassword : null;
		console.log('updating->',this.profile);
		this.data.updateRecord('user',filter,this.profile).subscribe(response=>{ 
			if(response == 1)
			{
				this.msg.addMessage('Updated Successfully');
			}
			this.profile.id = filter.id;
		});
	}
}
