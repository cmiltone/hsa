import { TestBed, inject } from '@angular/core/testing';

import { BookingResolverService } from './booking-resolver.service';

describe('BookingResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookingResolverService]
    });
  });

  it('should be created', inject([BookingResolverService], (service: BookingResolverService) => {
    expect(service).toBeTruthy();
  }));
});
