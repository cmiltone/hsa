import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

import { Observable } 	from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { Booking } from '../models/booking';
import { DataService } from '../services/data.service';
import { MessageService } from '../messaging/message.service';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class BookingResolverService implements Resolve<Booking[]>
{
	public redirectTo: string = '/dashboard';
	constructor(
		private data: DataService,
		private router: Router,
		private msg: MessageService,
		private auth: AuthService
	) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Booking[]>
	{
		let  filter = { provider_id: -1};
		let _bookings: Booking[] = [];
		if(this.auth.authToken.role === 'PROVIDER')
		{
			filter = { provider_id: this.auth.authToken.uid};
		}
		else if(this.auth.authToken.role == 'ADMIN')
		{
			filter = null;
		}
		
		return this.data.getRecords('client_order',filter).map(bookings=>{
			if(bookings)
			{
				bookings.forEach((booking:any)=>{
					_bookings.push(new Booking(booking));
				});
				console.log('resolving bookings->',_bookings);
				return _bookings;
			}
			else
			{
				this.msg.addMessage('You Have no new Orders');
				this.router.navigate([this.redirectTo]);
				return _bookings;
			}
		});
	}
}