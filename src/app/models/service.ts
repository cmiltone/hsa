import { User } from './user';
export class Service
{
	public id?: number;
	public label: string;
	public description: string;
	public service_fee: number;
	public image: string;
	public added_on: Date;
	public modified_on: Date;

	constructor(s:{
		label: string,
		description: string,
		added_on?: Date,
		image: string,
		modified_on?: Date,
		service_fee: number,
		id?: number,

	})
	{
		this.label = s.label;
		this.description = s.description;
		this.service_fee = s.service_fee;
		this.image = `../assets/service-imgs/${s.image}`;
		this.added_on = s.added_on ? s.added_on : new Date();
		this.modified_on = s.modified_on ? s.modified_on : new Date();
		this.id = s.id ? s.id : null;
	}
}