import { APIResponse } from './interfaces';
import { Service } from './service';


const services: Service[] = [
	new Service({ id: 1, service_fee: 0.00, label: 'Laundry', description: 'laundry desc', image:'placeholder.png' }),
	new Service({ id: 2, service_fee: 0.00, label: 'Gardening', description: 'gardening desc', image:'placeholder.png' }),
	new Service({ id: 3, service_fee: 0.00, label: 'Renovations', description: 'renovations desc e.g painting', image:'placeholder.png' }),
	new Service({ id: 4, service_fee: 0.00, label: 'Shopping', description: 'Shopping errands desc', image:'placeholder.png' }),
	new Service({ id: 5, service_fee: 0.00, label: 'Babysitting', description: 'Babysitting desc', image:'placeholder.png' }),
	new Service({ id: 6, service_fee: 0.00, label: 'Interior Decor', description: 'Interior decor desc', image:'placeholder.png'})
];

export const mockResponse: APIResponse = {
	data: null,
	error: null
};