import { User } from './user';
import { Provider } from './provider';
import { Service } from './service';
export class Booking
{
	public id?: number;
	public client_id?: number;
	public client: User;
	public provider_id: number;
	public provider: Provider;
	public product: Service;
	public status?: string;
	public add_date?: Date;
	public last_modified?: Date;

	constructor(b:{
		provider_id: number,
		client: string,
		product: string,
		provider: string,
		client_id?: number,
		status?: string,
		id?: number,
		add_date?: Date,
		last_modified?: Date,
	})
	{
		this.client_id = b.client_id;
		this.provider_id = b.provider_id;
		this.status = b.status ? b.status : null;
		this.add_date = b.add_date ? b.add_date : null;
		this.last_modified = b.last_modified ? b.last_modified : null;
		this.client = b.client ? this.parseProp(b.client) : null;
		this.product = b.product ? this.parseProp(b.product) : null;
		this.provider = b.provider ? this.parseProp(b.provider) : null;
		this.id = b.id ? b.id : null;
	}
	private parseProp(prop:string): any
	{
		return JSON.parse(prop);
	}
}
