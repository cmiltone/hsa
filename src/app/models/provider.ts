import { User } from './user';
export class Provider
{
	public id?: number;
	public user_id: number;
	public provider: User;
	public service_id: number;
	public cost: number;
	public add_date?: Date;
	public last_modified?: Date;

	constructor(p:{
		user_id: number,
		provider: string,
		service_id: number,
		cost: number,
		id?: number,
		add_date?: Date,
		last_modified?: Date,
	})
	{
		this.user_id = p.user_id;
		this.last_modified = p.last_modified ? p.last_modified : new Date();
		this.add_date = p.add_date ?p.add_date : new Date();
		this.provider = p.provider ? this.parseProp(p.provider) : null;
		this.service_id = p.service_id;
		this.cost = p.cost;
		this.id = p.id ? p.id : null;
	}

	private parseProp(prop:string):any
	{
		return JSON.parse(prop);
	}
}
