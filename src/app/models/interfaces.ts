export interface APIResponse {
	data: any;
	error: any;
}

export interface APIRequest {
	params: {
		a: string;//(action or) type of request, e.g: 'query'(for db queries eg delete, selesct), 'auth'( for auth requests)
		target: string;//cmd type;- e.g 'get', 'set' or 'reg', 'login'
		table: string;//table name;- e.g 'customers' or 'reg', 'login'
		filter?: any;// possible values: null or {} (eg {'id': 1}), used to filter records (optional)
		data?: {};//data to be saved (optional)
		tkn?: string;//auth key (optional only for auth auth quests except reg)
	};  
	request_msg: {
		req_desc: string;//description of request, e.g: 'Fetching Products', 'Saving Account details'
		success_msg: string;//success message for request, e.g: 'Fetched Products', 'Account details saved'
		error_msg?:any;//error message(s) (optional)
	}
	
}
