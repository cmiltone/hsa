export class User
{
	public id?: number;
	public fullname: string;
	public contact: string;
	public address: string;
	public role: string;
	public username?: string;
	public password?: string;
	public status: string;
	constructor(u:{
		fullname: string,
		contact: string,
		address: string,
		role: string,
		id?: number,
		username?: string,
		password?: string,
		status?: string
	})
	{
		this.fullname = u.fullname
		this.contact = u.contact
		this.address = u.address
		this.role = u.role
		this.id = u.id ? u.id : null;
		this.username = u.username ? u.username : null;
		this.password = u.password ? u.username : null;
		this.status = u.status ? u.status : 'ACTIVE';
	}
}
