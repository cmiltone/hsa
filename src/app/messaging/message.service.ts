import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {

	public errors: string[] = []; 
	public info: string[] = [];

	constructor() { }

	public addMessage(message: string)
	{
		this.info.push(message);
	}

	public clear()
	{
		this.info = [];
		this.errors = [];
	}

	public addErrorMessage(e_message: string)
	{
		this.errors.push(e_message);
	}

}
