webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("../../../../rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_mocks__ = __webpack_require__("../../../../../src/app/models/mocks.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

;





var options = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({
        'Content-Type': 'application/json',
        'Authorization': ''
    })
};
var ApiService = (function () {
    function ApiService(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.apiURL = 'backend/api.php';
    }
    ApiService.prototype.sendRequest = function (request) {
        var _this = this;
        // dummy result:- in case of http failure let the app continue
        var result = __WEBPACK_IMPORTED_MODULE_4__models_mocks__["a" /* mockResponse */];
        result.error = 'Could not get  a valid response from the Server: Network and/or Server Error!';
        //first check that data is ok
        if (request.params.data)
            request.params.data = this.prepData(request.params.data);
        //send the request and return response 
        return this.http.post(this.apiURL, request.params, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (response_payload) {
            if (request.request_msg.success_msg) {
                _this.log(request.request_msg.success_msg);
            }
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.errorHandler(request.request_msg.req_desc, result)));
    };
    /**
    * Handle failed operation.
    * Let app continue
    * @param operation - name of operation that failed
    * @param result - optional value to return as observable result
    */
    ApiService.prototype.errorHandler = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //TODO: send error to remote logging infrustructure
            console.log(error);
            //TODO: make error user friendly
            var error_text = 'Could not get  a valid response from the Server: Network and/or Server Error!';
            if (error.status) {
                error_text = error.status == 404 ? 'Could Not Connect to Server' : error_text;
            }
            _this.logError(operation + " failed: " + error_text);
            //let app keep running; return empty result
            return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(result);
        };
    };
    ApiService.prototype.log = function (message) {
        this.messageService.addMessage(message);
    };
    ApiService.prototype.logError = function (e) {
        this.messageService.addErrorMessage(e);
    };
    ApiService.prototype.prepData = function (d) {
        // TODO: Validate input data:- check for SQL injection and other maliciuos data
        return JSON.parse(JSON.stringify(d)); // remove undefined items
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app{\r\n\tposition: absolute;\r\n\ttop: 0; right: 0; left: 0; bottom: 0;\r\n\tbackground-image: url(" + escape(__webpack_require__("../../../../../src/assets/bg.jpg")) + ");\r\n}\r\n\r\n.app-header{\r\n\tposition: fixed;\r\n\ttop: 0; left: 0; right: 0;\r\n\theight: 90px;\r\n\tz-index: 999;\r\n\tbackground-color: #f3f6f6;\r\n}\r\n\r\n.header{\r\n\tpadding: 4px;\r\n\tmargin: 0;\r\n\t/*font-size: 1em*/\r\n}\r\n\r\n.app-nav{\r\n\tposition: absolute;\r\n\tbottom: 0;\r\n\tright: calc( 50% - 126px );\r\n\tleft: calc( 50% - 126px );\r\n\theight: auto;\r\n\t/*background-color: #fff;*/\r\n}\r\n\r\n.nav-items{\r\n\theight: auto;\r\n\tmargin: 0 auto;\r\n\tpadding: 0;\t\r\n}\r\n\r\n.nav-item{\r\n\twidth: 80px;\r\n\tlist-style-type: none;\r\n\tfloat: left;\r\n\ttext-align: center;\r\n\theight: 20px;\r\n\tbackground-color: #dce8e7;\r\n\tmargin: 2px;\r\n}\r\n\r\n.nav-item:hover{\r\n\tbackground-color: #dcffb3;\r\n}\r\n\r\n.dashboard-links{\r\n\tposition: absolute;\r\n\tz-index: 99;\r\n\ttop: 40px; right: 5px;\r\n\tmargin: 0;\r\n\tpadding: 0;\r\n}\r\n\r\n.dashboard-links > ul{\r\n\tborder: none;\r\n\tdisplay: none;\r\n\tclear: both;\r\n}\r\n\r\n.dashboard-links > ul > li{\r\n\ttext-align: center;\r\n\twidth: 90px;\r\n\tclear: both;\r\n}\r\n\r\n.dashboard-links:hover > ul{\r\n\tdisplay: block;\r\n}\r\n\r\n.dashboard-links:hover > span{\r\n\tdisplay: none;\r\n}\r\n\r\n.app-content-wrapper{\r\n\tposition: absolute;\r\n\ttop: 92px; right: 10px; left: 10px; bottom: 45px; \r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\tbackground-color: #fff;\r\n\toverflow: hidden;\r\n\toverflow-y: scroll;\r\n}\r\n\r\n.app-footer{\r\n\tposition: absolute;\r\n\theight: auto;\r\n\tbottom: 0; left: 10px; right: 10px;\r\n\tpadding: 2px;\r\n\ttext-align: center;\r\n\tbackground-color: #cdd5d5;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app\" *ngIf=\"auth.authToken; else loadingApp\">\n  <div class=\"app-header\">\n    <h1 class=\"header\">{{title}}</h1>\n\n    <ul class=\"dashboard-links\">\n      <li class=\"nav-item\">\n        <a class=\"link\" *ngIf=\"!auth.authToken.l\" routerLink=\"/auth\">Login</a>\n        <a class=\"link\" *ngIf=\"auth.authToken.l\" routerLink=\"/dashboard\">{{auth.authToken.uname}}</a>\n      </li>\n      <ul class=\"nav-items\">\n        <li class=\"nav-item\" *ngIf=\"auth.authToken.role=='ADMIN'\">\n            <a class=\"link\" routerLink=\"/dashboard/orders\">Orders</a>\n        </li>\n        <li class=\"nav-item\" *ngIf=\"auth.authToken.role=='ADMIN'\">\n            <a class=\"link\" routerLink=\"/dashboard/users\">Users</a>\n        </li>\n        <li class=\"nav-item\" *ngIf=\"auth.authToken.role=='ADMIN'\">\n            <a class=\"link\" routerLink=\"/dashboard/new\">Add Service</a>\n        </li>\n        <li class=\"nav-item\" *ngIf=\"auth.authToken.role=='PROVIDER'\">\n          <a class=\"link\" routerLink=\"/dashboard/orders/{{auth.authToken.uid}}\">Orders</a>\n        </li>\n        <li class=\"nav-item\" *ngIf=\"auth.authToken.l\"><a class=\"link\" href=\"/?logout=true\">Logout</a></li>\n      </ul>     \n    </ul>\n\n    <nav class=\"app-nav\">\n      <ul class=\"nav-items\">\n        <li class=\"nav-item\"><a class=\"link\" routerLink=\"/\">Home</a></li>\n        <li class=\"nav-item\"><a class=\"link\" routerLink=\"/services\">Services</a></li>\n        <li class=\"nav-item\"><a class=\"link\" routerLink=\"/about\">About</a></li>\n      </ul>\n    </nav>\n  </div>\n\n  <div class=\"app-content-wrapper\">\n    <router-outlet></router-outlet><!--  -->\n  </div>\n\n  <div class=\"app-footer\">\n    Copyright &copy; 2018, IF/XX/YY | Designed By Marion Jepchumba\n    <!-- <a href=\"https://bingwah.com\" class=\"link\" target=\"_blank\" hidden>Bingwah Systems</a> -->\n  </div>\n</div>\n<app-messaging></app-messaging>\n<ng-template #loadingApp><h1 align=\"center\" style=\"color: blue;\"><i>Loading...</i></h1></ng-template>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(auth) {
        var _this = this;
        this.auth = auth;
        this.title = 'Home Service System';
        this.auth.getAuthToken().subscribe(function (auth) {
            _this.auth.authToken = auth;
        });
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__auth_auth_service__["a" /* AuthService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__main_main_module__ = __webpack_require__("../../../../../src/app/main/main.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_services_module__ = __webpack_require__("../../../../../src/app/services/services.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_services_routing__ = __webpack_require__("../../../../../src/app/services/services.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_services_component__ = __webpack_require__("../../../../../src/app/services/services.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_auth_component__ = __webpack_require__("../../../../../src/app/auth/auth.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_profile_resolver_service__ = __webpack_require__("../../../../../src/app/services/profile-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__dashboard_dashboard_module__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__messaging_messaging_component__ = __webpack_require__("../../../../../src/app/messaging/messaging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__auth_auth_guard_service__ = __webpack_require__("../../../../../src/app/auth/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__auth_admin_guard_service__ = __webpack_require__("../../../../../src/app/auth/admin-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__image_service__ = __webpack_require__("../../../../../src/app/image.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







///////custom////
//modules


//routing


//components












var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__main_main_component__["a" /* MainComponent */],
                __WEBPACK_IMPORTED_MODULE_12__services_services_component__["a" /* ServicesComponent */],
                __WEBPACK_IMPORTED_MODULE_13__auth_auth_component__["a" /* AuthComponent */],
                __WEBPACK_IMPORTED_MODULE_19__messaging_messaging_component__["a" /* MessagingComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_9__app_routing__["a" /* AppRouting */],
                __WEBPACK_IMPORTED_MODULE_10__services_services_routing__["a" /* ServicesRouting */],
                __WEBPACK_IMPORTED_MODULE_7__main_main_module__["a" /* MainModule */],
                __WEBPACK_IMPORTED_MODULE_8__services_services_module__["a" /* ServicesModule */],
                __WEBPACK_IMPORTED_MODULE_18__dashboard_dashboard_module__["a" /* DashboardModule */]
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_5__angular_common__["g" /* LocationStrategy */], useClass: __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* HashLocationStrategy */] },
                __WEBPACK_IMPORTED_MODULE_14__auth_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_15__api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_16__messaging_message_service__["a" /* MessageService */], __WEBPACK_IMPORTED_MODULE_17__services_profile_resolver_service__["a" /* ProfileResolverService */], __WEBPACK_IMPORTED_MODULE_20__auth_auth_guard_service__["a" /* AuthGuardService */], __WEBPACK_IMPORTED_MODULE_21__auth_admin_guard_service__["a" /* AdminGuardService */], __WEBPACK_IMPORTED_MODULE_22__image_service__["a" /* ImageService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_auth_component__ = __webpack_require__("../../../../../src/app/auth/auth.component.ts");


var appRoutes = [
    {
        path: 'auth', component: __WEBPACK_IMPORTED_MODULE_1__auth_auth_component__["a" /* AuthComponent */]
    }
];
var AppRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forRoot(appRoutes);


/***/ }),

/***/ "../../../../../src/app/auth/admin-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminGuardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminGuardService = (function () {
    function AdminGuardService(auth, router, msg) {
        this.auth = auth;
        this.router = router;
        this.msg = msg;
    }
    AdminGuardService.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkRole(url);
    };
    AdminGuardService.prototype.checkRole = function (url) {
        if (this.auth.authToken) {
            if (this.auth.authToken.l && this.auth.authToken.role === 'ADMIN') {
                return true;
            }
            else
                this.msg.addMessage("Access to '" + url + "' Denied for Current User Type");
        }
        this.auth.setRedirect(url);
        this.router.navigate(['/auth']);
        return false;
    };
    AdminGuardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__messaging_message_service__["a" /* MessageService */]])
    ], AdminGuardService);
    return AdminGuardService;
}());



/***/ }),

/***/ "../../../../../src/app/auth/auth-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardService = (function () {
    function AuthGuardService(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkLogin(url);
    };
    AuthGuardService.prototype.checkLogin = function (url) {
        if (this.auth.authToken) {
            if (this.auth.authToken.l) {
                return true;
            }
            this.auth.setRedirect(url);
            this.router.navigate(['/auth']);
        }
        this.auth.setRedirect(url);
        this.router.navigate(['/auth']);
        return false;
    };
    AuthGuardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "../../../../../src/app/auth/auth.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-auth{\r\n\tpadding: 5px;\r\n\tmargin: 10px;\r\n}\r\n\r\n.auth{\r\n\tborder:1px solid #ccc;\r\n\tborder-radius: 4px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-auth\">\n\t<div class=\"auth\" *ngIf=\"!reg\">\n\t\t<h2>Login</h2>\n\t\t<form [formGroup]=\"lf\" #lForm=\"ngForm\" (ngSubmit)=\"login()\">\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"username\">User Name:</label>\n\t\t\t\t<input class=\"input-text\" id=\"username\" type=\"text\" name=\"username\"\n\t\t\t\t\tformControlName=\"username\" placeholder=\"Enter User Name\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"lf.get('username').valid || (lf.get('username').pristine && !lForm.submitted)\">User Name is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"password\">Password:</label>\n\t\t\t\t<input class=\"input-text\" id=\"password\" type=\"password\" name=\"password\"\n\t\t\t\t\tformControlName=\"password\" placeholder=\"Enter Password\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"lf.get('password').valid || (lf.get('password').pristine && !lForm.submitted)\">Password is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<button [disabled]=\"lForm.invalid\">Login</button>\n\t\t\t\tDon't Have an Account? <a class=\"link\" (click)=\"reg = !reg\">Register</a>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n\t<div class=\"auth\" *ngIf=\"reg\">\n\t\t<h2>Sign Up here to be Assigned Jobs</h2>\n\t\t<form [formGroup]=\"rf\" #rForm=\"ngForm\" (ngSubmit)=\"register()\">\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"fullname\">Full Name:</label>\n\t\t\t\t<input class=\"input-text\" id=\"fullname\" type=\"text\" name=\"full\"\n\t\t\t\t\tformControlName=\"fullname\" placeholder=\"Enter Full Name\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"rf.get('fullname').valid || (rf.get('fullname').pristine && !rForm.submitted)\">Full Name is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"contact\">Contact:</label>\n\t\t\t\t<input class=\"input-text\" id=\"contact\" type=\"text\" name=\"contact\"\n\t\t\t\t\tformControlName=\"contact\" placeholder=\"e.g phone or email\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"rf.get('contact').valid || (rf.get('contact').pristine && !rForm.submitted)\">Contact is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"address\">Address:</label>\n\t\t\t\t<input class=\"input-text\" id=\"address\" type=\"text\" name=\"address\"\n\t\t\t\t\tformControlName=\"address\" placeholder=\"Enter your Location\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"rf.get('address').valid || (rf.get('address').pristine && !rForm.submitted)\">Address is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"username\">User Name:</label>\n\t\t\t\t<input class=\"input-text\" id=\"username\" type=\"text\" name=\"lusername\"\n\t\t\t\t\tformControlName=\"username\" placeholder=\"Enter User Name\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"rf.get('username').valid || (rf.get('username').pristine && !rForm.submitted)\">User Name is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"password\">Password:</label>\n\t\t\t\t<input class=\"input-text\" id=\"password\" type=\"password\" name=\"password\"\n\t\t\t\t\tformControlName=\"password\" placeholder=\"Enter Password\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"rf.get('password').valid || (rf.get('password').pristine && !rForm.submitted)\">Password is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<button [disabled]=\"rForm.invalid\">Register</button>\n\t\t\t\tAlready have an Account? <a class=\"link\" (click)=\"reg = !reg\">Login</a>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthComponent = (function () {
    function AuthComponent(auth, messageService, router) {
        this.auth = auth;
        this.messageService = messageService;
        this.router = router;
        this.reg = false;
        this.lf = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            username: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
        this.rf = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            fullname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            contact: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            address: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            username: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
    }
    AuthComponent.prototype.ngOnInit = function () {
        var _this = this;
        //first check if is logged in
        this.auth.getAuthToken().subscribe(function (token) {
            if (token.l == true) {
                //this.messageService.addMessage('Already Logged in. Redirecting...');
                _this.router.navigate([_this.auth.redirectUrl]);
            }
        });
    };
    AuthComponent.prototype.login = function () {
        var user = this.lf.value;
        user.status = 'ACTIVE';
        this.auth.loginUser(user);
    };
    AuthComponent.prototype.register = function () {
        var user = this.rf.value;
        console.log(user);
        var msg = ['Registration', 'Registration request sent'];
        this.auth.registerUser(user, msg);
    };
    AuthComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-auth',
            template: __webpack_require__("../../../../../src/app/auth/auth.component.html"),
            styles: [__webpack_require__("../../../../../src/app/auth/auth.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ }),

/***/ "../../../../../src/app/auth/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




 /**/
var AuthService = (function () {
    function AuthService(api, router, messageService) {
        this.api = api;
        this.router = router;
        this.messageService = messageService;
        this.redirectUrl = '/';
        this.authError = '';
        this.authToken = null;
    }
    AuthService.prototype.registerUser = function (user, msg) {
        var _this = this;
        var payload = {
            params: { a: 'auth', target: "reg", table: "user", data: user },
            request_msg: { req_desc: msg[0], success_msg: msg[1] },
        };
        this.api.sendRequest(payload).subscribe(function (response) {
            if (response.data.length)
                _this.messageService.addMessage('Registration Successfull. Please Login');
            else if (response.error) {
                var e = JSON.stringify(response.error);
                _this.messageService.addErrorMessage(e);
            }
        });
    };
    AuthService.prototype.loginUser = function (user) {
        var _this = this;
        // attempt login
        var payload = {
            params: { a: 'auth', target: "login", table: "user", filter: user },
            request_msg: { req_desc: "Login ", success_msg: "Login request Sent" },
        };
        this.api.sendRequest(payload).subscribe(function (response) {
            if (response.error) {
                var e = JSON.stringify(response.error);
                _this.messageService.addErrorMessage(e);
                ;
            }
            else if (response.data.auth_token.l) {
                _this.authToken = response.data.auth_token;
                _this.router.navigate([_this.redirectUrl]);
                setTimeout(function () {
                    _this.messageService.clear();
                }, 5000);
            }
        });
    };
    AuthService.prototype.getAuthToken = function () {
        var payload = {
            params: { a: "auth", target: "get", table: null },
            request_msg: { req_desc: 'A request ', success_msg: "" },
        };
        return this.api.sendRequest(payload).map(function (response) { return response.data.auth_token; });
    };
    AuthService.prototype.setRedirect = function (url) {
        this.redirectUrl = url;
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__["a" /* MessageService */]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/account/account.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/account/account.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-account\" *ngIf=\"profile; else loadingProfile\">\n\t<h2>{{title}}</h2>\n\t<form [formGroup]=\"pf\" #pform=\"ngForm\" (ngSubmit)=\"update()\">\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"fullname\">Full Name:</label>\n\t\t\t<input class=\"input-text\" id=\"fullname\" type=\"text\" name=\"full\"\n\t\t\t\tformControlName=\"fullname\" [(ngModel)]=\"profile.fullname\" placeholder=\"Enter Full Name\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('fullname').valid || (pf.get('fullname').pristine && !pform.submitted)\">Full Name is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"contact\">Contact:</label>\n\t\t\t<input class=\"input-text\" id=\"contact\" type=\"text\" name=\"label\"\n\t\t\t\tformControlName=\"contact\" [(ngModel)]=\"profile.contact\" placeholder=\"e.g phone or email\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('contact').valid || (pf.get('contact').pristine && !pform.submitted)\">Contact is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"address\">Address:</label>\n\t\t\t<input class=\"input-text\" id=\"address\" type=\"text\" name=\"label\"\n\t\t\t\tformControlName=\"address\" [(ngModel)]=\"profile.address\" placeholder=\"Enter your Location\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('address').valid || (pf.get('address').pristine && !pform.submitted)\">Address is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"username\">User Name:</label>\n\t\t\t<input class=\"input-text\" id=\"username\" type=\"text\" name=\"label\"\n\t\t\t\tformControlName=\"username\" [(ngModel)]=\"profile.username\" placeholder=\"Enter User Name\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('username').valid || (pf.get('username').pristine && !pform.submitted)\">User Name is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"password\">New Password (Optional):</label>\n\t\t\t<input class=\"input-text\" id=\"npassword\" type=\"password\" name=\"npassword\"\n\t\t\t\tformControlName=\"npassword\" placeholder=\"Enter New Password\">\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('npassword').valid || (pf.get('npassword').pristine && !pform.submitted)\">Password is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"password\">Password:</label>\n\t\t\t<input class=\"input-text\" id=\"password\" type=\"password\" name=\"label\"\n\t\t\t\tformControlName=\"password\" placeholder=\"Enter Current Password\" required>\n\t\t\t<input type=\"hidden\" name=\"pass\" formControlName=\"pass\">\n\t\t\t<span class=\"form-error\" [hidden]=\"pf.get('password').valid || (pf.get('password').pristine && !pform.submitted)\">Password is required</span>\n\t\t\t<span class=\"form-error\" [hidden]=\"!pf.hasError('incorrect') || (pf.pristine && !pform.submitted)\">Account Password Invalid</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<button class=\"input-btn\" type=\"submit\" [disabled]=\"pform.invalid\">Update</button>\n\t\t\t<button class=\"input-btn\" type=\"button\" (click)=\"delete()\" [disabled]=\"pf.hasError('incorrect')\">Deactivate Account</button>\n\t\t</div>\n\t</form>\n</div>\n<ng-template #loadingProfile><i>Loading profile...</i></ng-template>"

/***/ }),

/***/ "../../../../../src/app/dashboard/account/account.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





function passwordValid(g) {
    return g.get('password').value === g.get('pass').value ? null : { 'incorrect': true };
}
var AccountComponent = (function () {
    function AccountComponent(data, route, msg) {
        this.data = data;
        this.route = route;
        this.msg = msg;
        this.pf = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            fullname: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required),
            contact: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required),
            address: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required),
            username: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required),
            pass: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */](),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required),
            npassword: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */](),
        }, passwordValid);
    }
    AccountComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.profile = data.profile;
            _this.pf.patchValue({ pass: _this.profile.password });
            _this.title = _this.profile.fullname + "'s Account Details";
        });
    };
    AccountComponent.prototype.delete = function () {
        var _this = this;
        var filter = { id: this.profile.id };
        this.profile.id = undefined;
        this.profile.status = 'INACTIVE';
        console.log('updating->', this.profile);
        this.data.updateRecord('user', filter, this.profile).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('Deactivated Successfully.');
            }
            _this.profile.id = filter.id;
        });
    };
    AccountComponent.prototype.update = function () {
        var _this = this;
        var filter = { id: this.profile.id };
        this.profile.id = undefined;
        this.pf.value.npassword ? this.profile.password = this.pf.value.npassword : null;
        console.log('updating->', this.profile);
        this.data.updateRecord('user', filter, this.profile).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('Updated Successfully');
            }
            _this.profile.id = filter.id;
        });
    };
    AccountComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-account',
            template: __webpack_require__("../../../../../src/app/dashboard/account/account.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/account/account.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_4__messaging_message_service__["a" /* MessageService */]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/booking-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingResolverService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_booking__ = __webpack_require__("../../../../../src/app/models/booking.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BookingResolverService = (function () {
    function BookingResolverService(data, router, msg, auth) {
        this.data = data;
        this.router = router;
        this.msg = msg;
        this.auth = auth;
        this.redirectTo = '/dashboard';
    }
    BookingResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var filter = { provider_id: -1 };
        var _bookings = [];
        if (this.auth.authToken.role === 'PROVIDER') {
            filter = { provider_id: this.auth.authToken.uid };
        }
        else if (this.auth.authToken.role == 'ADMIN') {
            filter = null;
        }
        return this.data.getRecords('client_order', filter).map(function (bookings) {
            if (bookings) {
                bookings.forEach(function (booking) {
                    _bookings.push(new __WEBPACK_IMPORTED_MODULE_4__models_booking__["a" /* Booking */](booking));
                });
                console.log('resolving bookings->', _bookings);
                return _bookings;
            }
            else {
                _this.msg.addMessage('You Have no new Orders');
                _this.router.navigate([_this.redirectTo]);
                return _bookings;
            }
        });
    };
    BookingResolverService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__messaging_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_7__auth_auth_service__["a" /* AuthService */]])
    ], BookingResolverService);
    return BookingResolverService;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/booking/booking.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-booking-list{\r\n\tpadding: 5px;\r\n}\r\n\r\n.booking-list-header{\r\n\ttext-align: center;\r\n}\r\n\r\n.booking-list-nav{\r\n\tmargin: 0 auto;\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\theight: 90px;\r\n}\r\n\r\n.booking-list{\r\n\tpadding: 0;\r\n\tmargin: 0;\r\n\tlist-style-type: none;\r\n}\r\n\r\n.booking-item{\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\tmargin: 4px;\r\n\tpadding: 5px;\r\n\tfloat: left;\r\n\theight: auto;\r\n\tmin-height: 150px;\r\n\twidth: 280px;\r\n}\r\n\r\n.booking-item:hover{\r\n\tbackground-color: #dfdfdf;\r\n}\r\n\r\n.booking-list-nav{\r\n\ttext-align: center;\r\n}\r\n\r\n.update-form{\r\n\tbackground-color: #fff;\r\n\theight: 120px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/booking/booking.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-booking-list\" *ngIf=\"orders\">\n\t<h2 class=\"booking-list-header\">{{title}}</h2>\n\t<nav class=\"booking-list-nav\">\n\t\t<input type=\"search\" class=\"input-text\" name=\"search_order\" placeholder=\"Search Orders\">\n\t\t<a class=\"link\" *ngIf=\"orders_link && !printed_id; else prntOrders\" target=\"_blank\" \n\t\t\thref=\"{{orders_link}}\">View Printed Orders' Details</a>\n\t\t<ng-template #prntOrders><button class=\"input-btn\"\n\t\t\t(click)=\"printOrders('all')\">Save all Orders' Details(as PDF)</button></ng-template>\n\t</nav>\n\t<ul *ngIf=\"orders.length; else loadingorders\" class=\"booking-list\">\n\t\t<li *ngFor=\"let order of orders\" class=\"booking-item\">\n\t\t\t<h3><a class=\"link\" routerLink=\"/services/{{order.product.id}}\">{{order.product.label}}</a></h3><hr width=\"90%\" align=\"center\">\n\t\t\t<p>service Fee Kshs.: {{order.product.service_fee}}</p>\t\n\t\t\t<p>Order Status: {{order.status}}</p>\n\t\t\t<p><b>Client Details</b><br>Name: {{order.client.fullname}}<br>Address: {{order.client.address}}<br>Contact: {{order.client.contact}}</p>\n\t\t\t<button type=\"button\" class=\"input-btn\"\n\t\t\t\t*ngIf=\"!selected_id\" (click)=\"selected_id = order.id\">Update Order</button>\n\t\t\t<form class=\"update-form\"\n\t\t\t\t[formGroup]=\"bf\" #bform=\"ngForm\" (ngSubmit)=\"update()\" *ngIf=\"selected_id === order.id\">\n\t\t\t\t<div class=\"form-field\">\n\t\t\t\t\t<label>Update Status:</label>\n\t\t\t\t\t<select formControlName=\"status\" [(ngModel)]=\"order.status\">\n\t\t\t\t\t\t<option value=\"UNKNOWN\">- Select Status -</option>\n\t\t\t\t\t\t<option value=\"COMPLETED\">Completed</option>\n\t\t\t\t\t\t<option value=\"SUSPENDED\">Suspended</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<button class=\"input-btn\" type=\"submit\" [disabled]=\"bf.inavlid\">Update</button>\n\t\t\t\t<button class=\"input-btn\" type=\"button\" (click)=\"selected_id = null\">Cancel/Close</button>\n\t\t\t</form>\n\t\t\t<h4>\n\t\t\t\t<a class=\"link\" *ngIf=\"orders_link && printed_id == order.id; else prntOrder\"\n\t\t\t\t\ttarget=\"_blank\" href=\"{{orders_link}}\">View Printed Order's Details</a>\n\t\t\t\t<ng-template #prntOrder><button class=\"input-btn\"\n\t\t\t\t\t(click)=\"printOrders(order.id)\">Save Order Details(as PDF)</button></ng-template>\n\t\t\t</h4>\n\t\t</li>\n\t</ul>\n\t<ng-template #loadingorders><i>Loading orders.. </i></ng-template>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard/booking/booking.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var BookingComponent = (function () {
    function BookingComponent(route, data, msg, auth) {
        this.route = route;
        this.data = data;
        this.msg = msg;
        this.auth = auth;
        this.title = '';
        this.selected_id = null;
        this.printed_id = null;
    }
    BookingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bf = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            status: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
        this.title = "Orders";
        this.route.data.subscribe(function (data) {
            _this.orders = data.bookings;
        });
    };
    BookingComponent.prototype.update = function () {
        var _this = this;
        var filter = { id: this.selected_id };
        var record = this.bf.value;
        this.data.updateRecord('client_order', filter, record).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('Order Updated Successfully');
            }
        });
    };
    BookingComponent.prototype.printOrders = function (param) {
        var _this = this;
        var filter = this.auth.authToken.role === 'ADMIN' ? null : { provider_id: this.auth.authToken.uid };
        param === 'all' ? null : filter = { id: param };
        this.printed_id = param === 'all' ? null : param;
        var params = {
            msg: ['Printing Order(s)', 'Printing Order(s) Failed'],
            target: 'orders',
            table: 'client_order',
            filter: filter
        };
        this.data.printRecords(params).subscribe(function (filename) {
            if (filename.length) {
                _this.orders_link = filename;
            }
        });
    };
    BookingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-booking',
            template: __webpack_require__("../../../../../src/app/dashboard/booking/booking.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/booking/booking.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__["a" /* AuthService */]])
    ], BookingComponent);
    return BookingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-dashboard{\r\n\tmargin: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-dashboard\">\n\t<router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__update_service_update_service_component__ = __webpack_require__("../../../../../src/app/dashboard/update-service/update-service.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__dashboard_routing__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__account_account_component__ = __webpack_require__("../../../../../src/app/dashboard/account/account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__booking_booking_component__ = __webpack_require__("../../../../../src/app/dashboard/booking/booking.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__booking_resolver_service__ = __webpack_require__("../../../../../src/app/dashboard/booking-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__provider_provider_component__ = __webpack_require__("../../../../../src/app/dashboard/provider/provider.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__dashboard_routing__["a" /* DashboardRouting */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__dashboard_component__["a" /* DashboardComponent */], __WEBPACK_IMPORTED_MODULE_5__update_service_update_service_component__["a" /* UpdateServiceComponent */], __WEBPACK_IMPORTED_MODULE_7__account_account_component__["a" /* AccountComponent */], __WEBPACK_IMPORTED_MODULE_8__booking_booking_component__["a" /* BookingComponent */], __WEBPACK_IMPORTED_MODULE_10__provider_provider_component__["a" /* ProviderComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_9__booking_resolver_service__["a" /* BookingResolverService */]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__account_account_component__ = __webpack_require__("../../../../../src/app/dashboard/account/account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__booking_booking_component__ = __webpack_require__("../../../../../src/app/dashboard/booking/booking.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__booking_resolver_service__ = __webpack_require__("../../../../../src/app/dashboard/booking-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__update_service_update_service_component__ = __webpack_require__("../../../../../src/app/dashboard/update-service/update-service.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__provider_provider_component__ = __webpack_require__("../../../../../src/app/dashboard/provider/provider.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_service_resolver_service__ = __webpack_require__("../../../../../src/app/services/service-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_profile_resolver_service__ = __webpack_require__("../../../../../src/app/services/profile-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_auth_guard_service__ = __webpack_require__("../../../../../src/app/auth/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__auth_admin_guard_service__ = __webpack_require__("../../../../../src/app/auth/admin-guard.service.ts");











var dashboardRoutes = [
    /*{ path: '**', redirectTo: 'dashboard' },*/
    {
        path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_1__dashboard_component__["a" /* DashboardComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_9__auth_auth_guard_service__["a" /* AuthGuardService */]],
        children: [
            {
                path: '', redirectTo: 'account', pathMatch: 'full'
            },
            {
                path: 'orders', component: __WEBPACK_IMPORTED_MODULE_3__booking_booking_component__["a" /* BookingComponent */],
                resolve: { bookings: __WEBPACK_IMPORTED_MODULE_4__booking_resolver_service__["a" /* BookingResolverService */] },
                canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_admin_guard_service__["a" /* AdminGuardService */]]
            },
            {
                path: 'users', component: __WEBPACK_IMPORTED_MODULE_6__provider_provider_component__["a" /* ProviderComponent */],
                canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_admin_guard_service__["a" /* AdminGuardService */]]
            },
            {
                path: 'orders/:provider_id', component: __WEBPACK_IMPORTED_MODULE_3__booking_booking_component__["a" /* BookingComponent */],
                resolve: { bookings: __WEBPACK_IMPORTED_MODULE_4__booking_resolver_service__["a" /* BookingResolverService */] }
            },
            {
                path: 'new', component: __WEBPACK_IMPORTED_MODULE_5__update_service_update_service_component__["a" /* UpdateServiceComponent */],
                canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_admin_guard_service__["a" /* AdminGuardService */]]
            },
            {
                path: 'update/:service_id', component: __WEBPACK_IMPORTED_MODULE_5__update_service_update_service_component__["a" /* UpdateServiceComponent */],
                resolve: { service: __WEBPACK_IMPORTED_MODULE_7__services_service_resolver_service__["a" /* ServiceResolverService */] }
            },
            {
                path: 'account', component: __WEBPACK_IMPORTED_MODULE_2__account_account_component__["a" /* AccountComponent */],
                resolve: { profile: __WEBPACK_IMPORTED_MODULE_8__services_profile_resolver_service__["a" /* ProfileResolverService */] }
            }
        ]
    },
];
var DashboardRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forChild(dashboardRoutes);


/***/ }),

/***/ "../../../../../src/app/dashboard/provider/provider.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-provider{\r\n\tpadding: 5px;\r\n}\r\n\r\n.provider-header{\r\n\ttext-align: center;\r\n}\r\n\r\n.provider-nav{\r\n\tmargin: 0 auto;\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\theight: 90px;\r\n}\r\n\r\n.provider-list{\r\n\tpadding: 0;\r\n\tmargin: 0;\r\n\tlist-style-type: none;\r\n}\r\n\r\n.provider-item{\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\tmargin: 4px;\r\n\tpadding: 5px;\r\n\tfloat: left;\r\n\theight: auto;\r\n\tmin-height: 150px;\r\n\twidth: 280px;\r\n}\r\n\r\n.provider-item:hover{\r\n\tbackground-color: #dfdfdf;\r\n}\r\n\r\n.provider-nav{\r\n\ttext-align: center;\r\n}\r\n\r\n.update-form{\r\n\tbackground-color: #fff;\r\n\theight: 200px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/provider/provider.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-provider\" *ngIf=\"users\">\n\t<h2 class=\"provider-header\">{{title}}</h2>\n\t<nav class=\"provider-nav\">\n\t\t<input type=\"search\" class=\"input-text\" name=\"search_user\" placeholder=\"Search Users\">\n\t\t<a class=\"link\" *ngIf=\"users_link && !printed_id; else prntUsers\" target=\"_blank\" \n\t\t\thref=\"{{users_link}}\">View Printed Users' Details</a>\n\t\t<ng-template #prntUsers><button class=\"input-btn\"\n\t\t\t(click)=\"printProviders('all')\">Save all Providers' Details(as PDF)</button></ng-template>\n\t</nav>\n\t<ul *ngIf=\"users.length; else loadingUsers\" class=\"provider-list\">\n\t\t<li *ngFor=\"let user of users\" class=\"provider-item\">\n\t\t\t<h3>{{user.username}}</h3><hr width=\"90%\" align=\"center\">\n\t\t\t<p>Role: {{user.role}}</p>\t\n\t\t\t<p>User Account Status: {{user.status}}</p>\n\t\t\t<p><b>User Details</b><br>Name: {{user.fullname}}<br>Address: {{user.address}}<br>Contact: {{user.contact}}</p>\n\t\t\t<button type=\"button\" class=\"input-btn\"\n\t\t\t\t[hidden]=\"selected_id\"\n\t\t\t\t[disabled]=\"user.id == auth.authToken.uid\"\n\t\t\t\t(click)=\"selected_id = user.id\">Update User's Account</button>\n\t\t\t<form class=\"update-form\"\n\t\t\t\t[formGroup]=\"uf\" (ngSubmit)=\"update()\" *ngIf=\"selected_id === user.id\">\n\t\t\t\t<div class=\"form-field\">\n\t\t\t\t\t<label>Update Role:</label>\n\t\t\t\t\t<select name=\"role\" formControlName=\"role\" [(ngModel)]=\"user.role\">\n\t\t\t\t\t\t<option value=\"UNKNOWN\">- Select Role -</option>\n\t\t\t\t\t\t<option value=\"PROVIDER\">Provider</option>\n\t\t\t\t\t\t<option value=\"ADMIN\">ADMIN</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-field\">\n\t\t\t\t\t<label>Update Account Status:</label>\n\t\t\t\t\t<select name=\"status\" formControlName=\"status\" [(ngModel)]=\"user.status\">\n\t\t\t\t\t\t<option value=\"UNKNOWN\">- Select Status -</option>\n\t\t\t\t\t\t<option value=\"ACTIVE\">Active</option>\n\t\t\t\t\t\t<option value=\"INACTIVE\">Inactive</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<button class=\"input-btn\" type=\"submit\" [disabled]=\"uf.inavlid\">Update</button>\n\t\t\t\t<button class=\"input-btn\" type=\"button\" (click)=\"selected_id = null\">Cancel/Close</button>\n\t\t\t</form>\n\t\t\t<h4>\n\t\t\t\t<a class=\"link\" *ngIf=\"users_link && printed_id == user.id; else prntUser\"\n\t\t\t\t\ttarget=\"_blank\" href=\"{{users_link}}\">View Printed User's Details</a>\n\t\t\t\t<ng-template #prntUser><button class=\"input-btn\" *ngIf=\"user.role == 'PROVIDER'\"\n\t\t\t\t\t(click)=\"printProviders(user.id)\">Save Provider's Details(as PDF)</button></ng-template>\n\t\t\t</h4>\n\t\t</li>\n\t</ul>\n\t<ng-template #loadingUsers><i>Loading User Accounts... </i></ng-template>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard/provider/provider.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProviderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProviderComponent = (function () {
    function ProviderComponent(route, data, msg, auth) {
        this.route = route;
        this.data = data;
        this.msg = msg;
        this.auth = auth;
        this.title = '';
        this.users = [];
        this.selected_id = null;
        this.printed_id = null;
        this.uf = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            status: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            role: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
    }
    ProviderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.title = "Manage User Accounts";
        var filter = null;
        this.data.getRecords('user', filter).subscribe(function (users) {
            if (users) {
                _this.users = users;
            }
        });
    };
    ProviderComponent.prototype.update = function () {
        var _this = this;
        var filter = { id: this.selected_id };
        var record = this.uf.value;
        this.data.updateRecord('user', filter, record).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('User Account Updated Successfully');
            }
        });
    };
    ProviderComponent.prototype.printProviders = function (param) {
        var _this = this;
        var filter = param === 'all' ? null : { user_id: param };
        this.printed_id = param === 'all' ? null : param;
        var params = {
            msg: ["Printing Provider(s)' Details", "Printing Provider(s)' Details Failed"],
            target: 'providers',
            table: 'service_provider',
            filter: filter
        };
        this.data.printRecords(params).subscribe(function (filename) {
            if (filename.length) {
                _this.users_link = filename;
            }
        });
    };
    ProviderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-provider',
            template: __webpack_require__("../../../../../src/app/dashboard/provider/provider.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/provider/provider.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__["a" /* AuthService */]])
    ], ProviderComponent);
    return ProviderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/update-service/update-service.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-services-update-detail{\r\n\tpadding: 5px;\r\n}\r\n\r\n.update-detail-header{\r\n\ttext-align: center;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/update-service/update-service.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-services-update-detail\" *ngIf=\"service; else loadingService\">\n\t<h3 class=\"update-detail-header\">{{service.label}}</h3>\n\t<form class=\"update-detail-form\" [formGroup]=\"f\" #sForm=\"ngForm\" (ngSubmit)=\"submit()\" enctype=\"maltipart/form-data\">\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"label\">Label/Title*:</label>\n\t\t\t<input class=\"input-text\" id=\"label\" type=\"text\" name=\"label\"\n\t\t\t\t[(ngModel)]=\"service.label\" formControlName=\"label\" placeholder=\"Enter the Label/Title of the service\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"f.get('label').valid || (f.get('label').pristine && !sForm.submitted)\">Title is required</span>\n\t\t</div>\n\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"cost\">Cost KSHS*:</label>\n\t\t\t<input class=\"input-text\" id=\"cost\" type=\"number\" name=\"cost\"\n\t\t\t\t[(ngModel)]=\"service.service_fee\" formControlName=\"service_fee\" placeholder=\"Cost in KSHS\" required>\n\t\t\t<span class=\"form-error\" [hidden]=\"f.get('service_fee').valid || (f.get('service_fee').pristine && !sForm.submitted)\">Cost is required</span>\n\t\t</div>\n\n\t\t<div class=\"form-field-textarea\">\n\t\t\t<label class=\"input-label\" for=\"desc\">Description*:</label>\n\t\t\t<textarea id=\"desc\" name=\"desc\"\n\t\t\t\tformControlName=\"description\"\n\t\t\t\t[(ngModel)]=\"service.description\" placeholder=\"Features or Intent\" required></textarea>\n\t\t\t<span class=\"form-error\" [hidden]=\"f.get('description').valid || (f.get('description').pristine && !sForm.submitted)\">Description is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"image-fiel\">Image (.png, .jpeg or .jpg)*:</label>\n\t\t\t<input type=\"hidden\" id=\"image\" name=\"image\" formControlName=\"image\"\n\t\t\t\t[(ngModel)]=\"service.image\">\n\t\t\t<input type=\"file\" id=\"image-file\" name=\"image-file\" accept=\"image/*\"\n\t\t\t\t(change)=\"uploadImage($event.target.files)\">\n\t\t\t<span class=\"form-error\" [hidden]=\"f.get('image').valid || (f.get('image').pristine && !sForm.submitted)\">A valid image is required!</span>\n\t\t</div>\n\t\t<div class=\"img-field\" *ngIf=\"service.image\">\n\t\t\t<img class=\"service_img\" src=\"../assets/service-imgs/{{service.image}}\" alt=\"{{service.label}}\">\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<button class=\"input-btn\" *ngIf=\"service.id\" (click)=\"fAction = 'delete'\">Delete Service</button>\n\t\t\t<button class=\"input-btn\" *ngIf=\"service.id\" (click)=\"fAction = 'update'\"\n\t\t\t\t[disabled]=\"sForm.invalid\">Update Service</button>\n\t\t\t<button class=\"input-btn\" *ngIf=\"!service.id\" (click)=\"fAction = 'add'\"\n\t\t\t\t[disabled]=\"sForm.invalid\">Add Service</button>\n\t\t</div>\n\t</form>\n</div>\n<ng-template #loadingService><i>Loading Service...</i></ng-template>"

/***/ }),

/***/ "../../../../../src/app/dashboard/update-service/update-service.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateServiceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_service__ = __webpack_require__("../../../../../src/app/models/service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__image_service__ = __webpack_require__("../../../../../src/app/image.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateServiceComponent = (function () {
    function UpdateServiceComponent(route, router, data, msg, imageService) {
        this.route = route;
        this.router = router;
        this.data = data;
        this.msg = msg;
        this.imageService = imageService;
        this.f = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            label: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            service_fee: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            description: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            image: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
    }
    UpdateServiceComponent.prototype.ngOnInit = function () {
        console.log('snapshot.url->', this.route.snapshot.url);
        if (this.route.snapshot.url[0].path == 'new') {
            this.service = new __WEBPACK_IMPORTED_MODULE_3__models_service__["a" /* Service */]({
                label: 'New',
                description: null,
                service_fee: 0.00,
                image: 'placeholder.png'
            });
            //this.service.image = 'placeholder.png';
        }
        else {
            this.getService();
        }
        this.service.image = this.service.image.split('/')[3];
    };
    UpdateServiceComponent.prototype.getService = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.service = data.service;
        });
    };
    UpdateServiceComponent.prototype.uploadImage = function (images) {
        var _this = this;
        console.log(images);
        if (images.length) {
            var image_1 = images[0];
            // upload image code
            var imageData = new FormData();
            imageData.append('image', image_1, image_1.name);
            this.imageService.sendFile(imageData).subscribe(function (response) {
                console.log(response);
                _this.service.image = image_1.name;
            });
        }
    };
    UpdateServiceComponent.prototype.submit = function () {
        console.log('saving->', this.f.value);
        switch (this.fAction) {
            case "add":
                {
                    this.save(this.f.value);
                }
                break;
            case "update":
                {
                    this.update(this.f.value);
                }
                break;
            case "delete":
                {
                    this.deleteService(this.f.value);
                }
                break;
            default:
                // code...
                break;
        }
    };
    UpdateServiceComponent.prototype.save = function (service) {
        var _this = this;
        this.data.addRecord('service', service).subscribe(function (response) {
            if (response) {
                _this.msg.addMessage('Service Saved successfully');
                _this.router.navigate(['dashboard', 'update', response]);
            }
        });
    };
    UpdateServiceComponent.prototype.update = function (service) {
        var _this = this;
        var filter = { id: this.service.id };
        this.data.updateRecord('service', filter, service).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('Service Updated successfully');
                _this.router.navigate(['dashboard', 'update', _this.service.id]);
            }
        });
    };
    UpdateServiceComponent.prototype.deleteService = function (service) {
        var _this = this;
        var filter = { id: this.service.id };
        this.data.deleteRecord('service', filter).subscribe(function (response) {
            if (response == 1) {
                _this.msg.addMessage('Service deleted successfully');
                _this.router.navigate(['services']);
            }
        });
    };
    UpdateServiceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-update-service',
            template: __webpack_require__("../../../../../src/app/dashboard/update-service/update-service.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/update-service/update-service.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_6__image_service__["a" /* ImageService */]])
    ], UpdateServiceComponent);
    return UpdateServiceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/image.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("../../../../rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_mocks__ = __webpack_require__("../../../../../src/app/models/mocks.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

;





var options = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]()
};
var ImageService = (function () {
    function ImageService(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.apiURL = 'backend/api.php';
    }
    ImageService.prototype.sendFile = function (image) {
        var _this = this;
        // dummy result:- in case of http failure let the app continue
        var result = __WEBPACK_IMPORTED_MODULE_4__models_mocks__["a" /* mockResponse */];
        result.error = 'Could not get  a valid response from the Server: Network and/or Server Error!';
        //first check that data is ok
        //send the request and return response 
        return this.http.post(this.apiURL, image, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (response_payload) { _this.log('Image Upload Request Sent'); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.errorHandler('Image Upload Failed', result)));
    };
    /**
    * Handle failed operation.
    * Let app continue
    * @param operation - name of operation that failed
    * @param result - optional value to return as observable result
    */
    ImageService.prototype.errorHandler = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //TODO: send error to remote logging infrustructure
            console.log(error);
            //TODO: make error user friendly
            var error_text = 'Could not get  a valid response from the Server: Network and/or Server Error!';
            if (error.status) {
                error_text = error.status == 404 ? 'Could Not Connect to Server' : error_text;
            }
            _this.logError(operation + " failed: " + error_text);
            //let app keep running; return empty result
            return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(result);
        };
    };
    ImageService.prototype.log = function (message) {
        this.messageService.addMessage(message);
    };
    ImageService.prototype.logError = function (e) {
        this.messageService.addErrorMessage(e);
    };
    ImageService.prototype.prepData = function (d) {
        // TODO: Validate input data:- check for SQL injection and other maliciuos data
        return JSON.parse(JSON.stringify(d)); // remove undefined items
    };
    ImageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */]])
    ], ImageService);
    return ImageService;
}());



/***/ }),

/***/ "../../../../../src/app/main/about/about.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-main-about{\r\n\tpadding: 5px;\r\n}\r\n\r\n.about-header{\r\n\ttext-align: center;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-main-about\">\n\t<h2 class=\"about-header\">About Us</h2>\n\t<p>\n\t\tHome Service System is a platform where people can transact online, request for services at the convenience of their homes. The services include house ronovation, laundry, electrical services and mechanical services Our clients are among home ownerstenants and organizations. Our company has trained personnel who perform all these tasks\n\t</p>\n\t<h3>Vision</h3>\n\t<p>To provide a wide access and world-class  services that delights our customers.</p>\n\t<h3>Mission</h3>\n\t<p>To provide quality and timely services to users.</p>\n\t<h3>Core values</h3>\n\t<ul>\n\t\t<li>Customer first</li>\n\t\t<li>One team</li>\n\t\t<li>Passion</li>\n\t\t<li>Integrity</li>\n\t\t<li>Excellence</li>\n\t</ul>\n\t<h3>Quality policy</h3>\n\t<p>\n\t\tWe are committed to providing high quality customer service by efficiently providing qualified personnel to perform tasks for our customers.The board, management and staff of house service system are committed to effective implementation and continual improvement of quality management system in order to consistently meet its customers and stakeholders requirements and expectations.\n\t</p>\n\t<h2>Contact Us</h2>\n\t<ul class=\"contact-items\">\n\t\t<li class=\"contact\"><a class=\"link\" target=\"_blank\" href=\"mailto:marionjepchumba@gmail.com\">Email Us</a></li>\n\t\t<li class=\"contact\"><a class=\"link\" target=\"_blank\" href=\"https://facebook.com/maryelymo\">Facebook</a></li>\n\t\t<li class=\"contact\"><a class=\"link\" target=\"_blank\" href=\"https://twitter.com/marionlimo\">Twitter</a></li>\n\t</ul>\n</div>\t\t"

/***/ }),

/***/ "../../../../../src/app/main/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-about',
            template: __webpack_require__("../../../../../src/app/main/about/about.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/about/about.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-main-home{\r\n\tpadding: 5px;\r\n}\r\n\r\n.home-header{\r\n\ttext-align: center;\r\n}\r\n\r\ndiv.slider-wrapper{\r\n\tborder: 4px solid #486260;\r\n    margin-left: 15%;\r\n    margin-right: 15%;\r\n    /*margin: 8px auto;*/\r\n    box-shadow: 5px 5px 5px #888888;\r\n}\r\n\r\ndiv.img-container{\r\n    width: 100%;\r\n    padding-bottom: 62.5%;\r\n    position: relative;\r\n    overflow: hidden;\r\n\r\n}\r\n\r\n.social{\r\n    height: 110px;\r\n    margin: 10px auto;\r\n    max-width: 330px;\r\n}\r\n\r\n.icon-link{\r\n    \r\n}\r\n\r\n.icon{\r\n    height: 100px;\r\n    width: 100px;\r\n    float: left;\r\n}\r\n\r\ndiv.img-container > div{\r\n    bottom: 0; left: 0;\r\n    position: absolute; right: 0; top: 0; \r\n\r\n}\r\n\r\n.holder{\r\n    width: 100%;\r\n    width: 100%;\r\n}\r\n\r\ndiv.show-slide > img:hover + a, a.slide-title:hover {\r\n    opacity: 0.7;\r\n    height: 40px;\r\n}\r\n\r\n.slide-div{\r\n    position: absolute;\r\n    display: none;\r\n    width: 100%;\r\n    height: 100%;\r\n\r\n}\r\n\r\n.show-slide{\r\n    display: block;\r\n}\r\n\r\n.slider-img{\r\n    position: absolute;\r\n\tmax-height: 100%;\r\n\tmax-width: 100%;\r\n}\r\n\r\n.slide-title{\r\n    position: absolute;\r\n    bottom: 0;\r\n    height: 20px;\r\n    right: 0;\r\n    left: 0;\r\n    background-color: #000;/*#989898*/\r\n    color: #ffffff;\r\n    opacity: 0.7;\r\n    display: block;\r\n    text-align: center;\r\n}\r\n\r\n.prev, .next{\r\n    cursor: pointer;\r\n    width: 20px;\r\n    height: 30px;\r\n    border-radius: 3px;\r\n    position: relative;\r\n    bottom: 1%;\r\n    text-align: center;\r\n    font-size: x-large;\r\n    color: #009688;\r\n    display: block;\r\n\r\n}\r\n\r\n.prev{\r\n    float: left;\r\n}\r\n\r\n.next{\r\n    float: right;\r\n}\r\n\r\n.prev:hover, .next:hover {\r\n    background-color: #ffffff; \r\n    color: #009688; \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-main-home\">\n\t<h2 class=\"home-header\">{{title}}</h2>\n\t<div class=\"slider-wrapper\">\n\t\t<div class=\"img-container\">\n\t\t\t<div>\n\t\t\t\t<div class=\"holder\">\n\t\t\t\t\t<div class=\"slide-div\" *ngFor=\"let slide of slides; let i = index\" [ngClass]=\"{'show-slide': i === currentIndex}\">\n\t\t\t\t\t\t<img class=\"slider-img\" src=\"assets/gallery/{{slide.image}}\" alt=\"{{slide.name}}\">\n\t\t\t\t\t\t<a class=\"link slide-title\" (click)=\"gotoServices()\">{{slide.name}}</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div id=\"prev-slide\" class=\"prev\" title=\"Previous\" (click)=\"prevSlide()\">&#8678;</div>\n\t\t<div id=\"next-slide\" class=\"next\" title=\"Next\" (click)=\"nextSlide()\">&#8680;</div>\n\t</div>\n\t<div class=\"social\">\n\t\t<a href=\"https://fb.com/maryelymo\" target=\"_blank\" class=\"link icon-link\"><img class=\"icon\" src=\"assets/icons/fb.png\" alt=\"Facebook\"></a>\n\t\t<a href=\"https://twitter.com/marionlimo\" target=\"_blank\" class=\"link icon-link\"><img class=\"icon\" src=\"assets/icons/twitter.png\" alt=\"Twitter\"></a>\n\t\t<a href=\"mailto:marionjepchumba@gmail.com\" target=\"_blank\" class=\"link icon-link\"><img class=\"icon\" src=\"assets/icons/gmail.png\" alt=\"Gmail\"></a>\n\t</div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = (function () {
    function HomeComponent(router) {
        this.router = router;
        this.title = "Popular Services";
        this.currentIndex = 0;
        this.slides = [
            { name: 'Babbysitting', image: 'babbysitting.jpeg' },
            { name: 'Cleaning', image: 'cleaning.jpeg' },
            { name: 'Interior Decor', image: 'decor.jpeg' },
            { name: 'Renovation', image: 'renovation.jpeg' },
            { name: 'Shopping', image: 'shopping.jpeg' },
        ];
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        setInterval(function () {
            _this.nextSlide();
        }, 5000);
    };
    HomeComponent.prototype.gotoServices = function () {
        this.router.navigate(['services']);
    };
    HomeComponent.prototype.nextSlide = function () {
        this.currentIndex++;
        if (this.currentIndex == this.slides.length) {
            this.currentIndex = 0;
        }
    };
    HomeComponent.prototype.prevSlide = function () {
        this.currentIndex--;
        if (this.currentIndex < 0) {
            this.currentIndex = this.slides.length - 1;
        }
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/main/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-home{\r\n\tmargin: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-home\">\n\t<router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainComponent = (function () {
    function MainComponent() {
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main',
            template: __webpack_require__("../../../../../src/app/main/main.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main_routing__ = __webpack_require__("../../../../../src/app/main/main.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home_component__ = __webpack_require__("../../../../../src/app/main/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about_about_component__ = __webpack_require__("../../../../../src/app/main/about/about.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MainModule = (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__main_routing__["a" /* MainRouting */] /**/
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__home_home_component__["a" /* HomeComponent */], __WEBPACK_IMPORTED_MODULE_4__about_about_component__["a" /* AboutComponent */]]
        })
    ], MainModule);
    return MainModule;
}());



/***/ }),

/***/ "../../../../../src/app/main/main.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home_component__ = __webpack_require__("../../../../../src/app/main/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__about_about_component__ = __webpack_require__("../../../../../src/app/main/about/about.component.ts");




var mainRoutes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_1__main_component__["a" /* MainComponent */],
        children: [
            { path: '', component: __WEBPACK_IMPORTED_MODULE_2__home_home_component__["a" /* HomeComponent */] },
            { path: 'about', component: __WEBPACK_IMPORTED_MODULE_3__about_about_component__["a" /* AboutComponent */] }
        ]
    },
];
var MainRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forChild(mainRoutes);


/***/ }),

/***/ "../../../../../src/app/messaging/message.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MessageService = (function () {
    function MessageService() {
        this.errors = [];
        this.info = [];
    }
    MessageService.prototype.addMessage = function (message) {
        this.info.push(message);
    };
    MessageService.prototype.clear = function () {
        this.info = [];
        this.errors = [];
    };
    MessageService.prototype.addErrorMessage = function (e_message) {
        this.errors.push(e_message);
    };
    MessageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], MessageService);
    return MessageService;
}());



/***/ }),

/***/ "../../../../../src/app/messaging/messaging.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-messaging{\r\n\tposition: fixed;\r\n\tbottom: 20px;\r\n\tright: 10px; left: 10px;\r\n\tz-index: 999;\r\n\topacity: 0.9;\r\n\tbackground-color: #FFECB3;\r\n\tborder-radius: 4px;\t\r\n\tpadding: 5px;\r\n}\r\n\r\n.messages{\r\n}\r\n\r\n.info{\r\n\tcolor: blue;\r\n\tfloat: left;\r\n\tclear: both;\r\n}\r\n.error{\r\n\tcolor: red;\r\n\tfloat: left;\r\n\tclear: both;\r\n}\r\n\r\n.clear-message{\r\n\tborder-radius: 10px;\r\n\tbackground-color: red;\r\n\tborder: none;\r\n\tfloat: right;\r\n\tfont-size: 1.2em;\r\n\tcursor: pointer;\r\n\tcolor: #fff;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/messaging/messaging.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-messaging\" *ngIf=\"messages.info.length || messages.errors.length\">\n\t<button class=\"clear-message\" (click)=\"messages.clear()\">&times;</button>\n\t<span class=\"info\" *ngFor=\"let info of messages.info\">{{info}}</span>\n\t<span class=\"error\" *ngFor=\"let error of messages.errors\">Error: {{error}}</span>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/messaging/messaging.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessagingComponent = (function () {
    function MessagingComponent(messages) {
        this.messages = messages;
    }
    MessagingComponent.prototype.ngOnInit = function () {
    };
    MessagingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-messaging',
            template: __webpack_require__("../../../../../src/app/messaging/messaging.component.html"),
            styles: [__webpack_require__("../../../../../src/app/messaging/messaging.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__message_service__["a" /* MessageService */]])
    ], MessagingComponent);
    return MessagingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/models/booking.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Booking; });
var Booking = (function () {
    function Booking(b) {
        this.client_id = b.client_id;
        this.provider_id = b.provider_id;
        this.status = b.status ? b.status : null;
        this.add_date = b.add_date ? b.add_date : null;
        this.last_modified = b.last_modified ? b.last_modified : null;
        this.client = b.client ? this.parseProp(b.client) : null;
        this.product = b.product ? this.parseProp(b.product) : null;
        this.provider = b.provider ? this.parseProp(b.provider) : null;
        this.id = b.id ? b.id : null;
    }
    Booking.prototype.parseProp = function (prop) {
        return JSON.parse(prop);
    };
    return Booking;
}());



/***/ }),

/***/ "../../../../../src/app/models/mocks.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mockResponse; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service__ = __webpack_require__("../../../../../src/app/models/service.ts");

var services = [
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 1, service_fee: 0.00, label: 'Laundry', description: 'laundry desc', image: 'placeholder.png' }),
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 2, service_fee: 0.00, label: 'Gardening', description: 'gardening desc', image: 'placeholder.png' }),
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 3, service_fee: 0.00, label: 'Renovations', description: 'renovations desc e.g painting', image: 'placeholder.png' }),
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 4, service_fee: 0.00, label: 'Shopping', description: 'Shopping errands desc', image: 'placeholder.png' }),
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 5, service_fee: 0.00, label: 'Babysitting', description: 'Babysitting desc', image: 'placeholder.png' }),
    new __WEBPACK_IMPORTED_MODULE_0__service__["a" /* Service */]({ id: 6, service_fee: 0.00, label: 'Interior Decor', description: 'Interior decor desc', image: 'placeholder.png' })
];
var mockResponse = {
    data: null,
    error: null
};


/***/ }),

/***/ "../../../../../src/app/models/provider.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Provider; });
var Provider = (function () {
    function Provider(p) {
        this.user_id = p.user_id;
        this.last_modified = p.last_modified ? p.last_modified : new Date();
        this.add_date = p.add_date ? p.add_date : new Date();
        this.provider = p.provider ? this.parseProp(p.provider) : null;
        this.service_id = p.service_id;
        this.cost = p.cost;
        this.id = p.id ? p.id : null;
    }
    Provider.prototype.parseProp = function (prop) {
        return JSON.parse(prop);
    };
    return Provider;
}());



/***/ }),

/***/ "../../../../../src/app/models/service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Service; });
var Service = (function () {
    function Service(s) {
        this.label = s.label;
        this.description = s.description;
        this.service_fee = s.service_fee;
        this.image = "../assets/service-imgs/" + s.image;
        this.added_on = s.added_on ? s.added_on : new Date();
        this.modified_on = s.modified_on ? s.modified_on : new Date();
        this.id = s.id ? s.id : null;
    }
    return Service;
}());



/***/ }),

/***/ "../../../../../src/app/services/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DataService = (function () {
    function DataService(api, message) {
        this.api = api;
        this.message = message;
    }
    DataService.prototype.getRecords = function (table, filter, msg) {
        var _this = this;
        msg = msg ? msg : ['Fetching Record(s)', 'Fetching Record(s) Failed'];
        var request = {
            params: { a: 'query', target: 'get', table: table, filter: filter },
            request_msg: {
                req_desc: msg[0],
                success_msg: '',
                error_msg: msg[1]
            }
        };
        return this.api.sendRequest(request).map(function (response) {
            if (response.error)
                _this.message.addErrorMessage(JSON.stringify(response.error));
            return response.data ? response.data : [];
        });
    };
    DataService.prototype.addRecord = function (table, record, msg) {
        var _this = this;
        msg = msg ? msg : ['Saving', 'Saving Record Failed'];
        var request = {
            params: { a: 'query', target: 'set', table: table, data: record },
            request_msg: {
                req_desc: msg[0],
                success_msg: 'Save request sent',
                error_msg: msg[1]
            }
        };
        return this.api.sendRequest(request).map(function (response) {
            if (response.error)
                _this.message.addErrorMessage(JSON.stringify(response.error));
            return response.data ? response.data : 0;
        });
    };
    DataService.prototype.deleteRecord = function (table, filter, msg) {
        var _this = this;
        msg = msg ? msg : ['Deleting Reocrd', 'Deleting Record Failed'];
        var request = {
            params: { a: 'query', target: 'delete', table: table, filter: filter },
            request_msg: {
                req_desc: msg[0],
                success_msg: 'Delete request sent',
                error_msg: msg[1]
            }
        };
        return this.api.sendRequest(request).map(function (response) {
            if (response.error)
                _this.message.addErrorMessage(JSON.stringify(response.error));
            return response.data ? response.data : 0;
        });
    };
    DataService.prototype.updateRecord = function (table, filter, data, msg) {
        var _this = this;
        msg = msg ? msg : ['Updating Record', 'Updating record Failed'];
        var request = {
            params: { a: 'query', target: 'update', table: table, filter: filter, data: data },
            request_msg: {
                req_desc: msg[0],
                success_msg: 'Update request sent',
                error_msg: msg[1]
            }
        };
        return this.api.sendRequest(request).map(function (response) {
            if (response.error)
                _this.message.addErrorMessage(JSON.stringify(response.error));
            return response.data ? response.data : 0;
        });
    };
    DataService.prototype.printRecords = function (params) {
        var _this = this;
        var request = {
            params: { a: 'print', target: params.target, table: params.table, filter: params.filter, data: null },
            request_msg: {
                req_desc: params.msg[0],
                success_msg: 'Print request sent',
                error_msg: params.msg[1]
            }
        };
        return this.api.sendRequest(request).map(function (response) {
            if (response.error)
                _this.message.addErrorMessage(JSON.stringify(response.error));
            return response.data ? response.data : '';
        });
    };
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3__messaging_message_service__["a" /* MessageService */]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "../../../../../src/app/services/profile-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileResolverService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfileResolverService = (function () {
    function ProfileResolverService(data, router, auth) {
        this.data = data;
        this.router = router;
        this.auth = auth;
        this.redirectTo = '/auth';
    }
    ProfileResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var filter = { id: +this.auth.authToken.uid };
        var msg = ['', 'Fetching account Details failed'];
        return this.data.getRecords('user', filter, msg).map(function (profiles) {
            if (profiles) {
                return profiles[0];
            }
            else {
                _this.router.navigate([_this.redirectTo]);
                return null;
            }
        });
    };
    ProfileResolverService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__["a" /* AuthService */]])
    ], ProfileResolverService);
    return ProfileResolverService;
}());



/***/ }),

/***/ "../../../../../src/app/services/service-detail/service-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-services-service-detail{\r\n\tpadding: 5px;\r\n}\r\n\r\n.service-detail-header{\r\n\ttext-align: center;\r\n}\r\n\r\n.service-detail{\r\n\tlist-style-type: none;\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n}\r\n\r\n.detail-item{\r\n\tmargin: 4px;\r\n\tpadding: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/services/service-detail/service-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-services-service-detail\" *ngIf=\"order && !requesting; else loadingService\">\n\t<h3 class=\"service-detail-header\">{{title}}</h3>\n\t<p>{{order.product.description}}</p>\n\t<ul class=\"service-detail\">\n\t\t<li class=\"detail-item\">Cost: Kshs. {{order.product.service_fee}}</li>\n\t</ul>\n\t<h4>Request Service here</h4>\n\t<form [formGroup]=\"f\" #bForm=\"ngForm\" (ngSubmit)=\"book()\" enctype=\"maltipart/form-data\">\n\t\t<div [formGroup]=\"f.get('client')\">\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"name\">Name*:</label>\n\t\t\t\t<input type=\"text\" name=\"name\" formControlName=\"fullname\" placeholder=\"Enter Your Full Name\"\n\t\t\t\t\t[(ngModel)]=\"order.client.fullname\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"f.get('client.fullname').valid || (f.get('client.fullname').pristine && !bForm.submitted)\">Full Name is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"contact\">Contact*:</label>\n\t\t\t\t<input type=\"text\" name=\"contact\" formControlName=\"contact\" placeholder=\"e.g phone no. or email\"\n\t\t\t\t\t[(ngModel)]=\"order.client.contact\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"f.get('client.contact').valid || (f.get('client.contact').pristine && !bForm.submitted)\">A valid Email or Phone No. is required</span>\n\t\t\t</div>\n\t\t\t<div class=\"form-field\">\n\t\t\t\t<label class=\"input-label\" for=\"address\">Address*:</label>\n\t\t\t\t<input type=\"text\" name=\"address\" formControlName=\"address\" placeholder=\"Enter your Location\"\n\t\t\t\t\t[(ngModel)]=\"order.client.address\" required>\n\t\t\t\t<span class=\"form-error\" [hidden]=\"f.get('client.address').valid || (f.get('client.address').pristine && !bForm.submitted)\">Address is required</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<label class=\"input-label\" for=\"provider\">Select Provider*:</label>\n\t\t\t<select id=\"provider\" name=\"provider\" formControlName=\"provider\" [(ngModel)]=\"order.provider\" required>\n\t\t\t\t<option *ngFor=\"let item of providers\" [ngValue]=\"item\">{{item.provider.fullname}} - KSHS  {{item.cost}}</option>\n\t\t\t</select>\n\t\t\t<span class=\"form-error\" [hidden]=\"f.get('provider').valid || (f.get('provider').pristine && !bForm.submitted)\">Provider is required</span>\n\t\t</div>\n\t\t<div class=\"form-field\">\n\t\t\t<input type=\"hidden\" name=\"product\" [(ngModel)]=\"order.product\" formControlName=\"product\">\n\t\t\t<button class=\"input-btn\" type=\"submit\" [disabled]=\"bForm.invalid\">Request Now</button>\n\t\t\t\n\t\t\t<a class=\"link\" *ngIf=\"order_link; else prntOrder\" target=\"_blank\"\n\t\t\t\t\thref=\"{{order_link}}\">Download Order Details</a>\n\t\t\t<ng-template #prntOrder>\n\t\t\t\t<button *ngIf=\"order_id\" class=\"input-btn\" type=\"button\" \n\t\t\t\t\t(click)=\"printOrder(order_id)\">Save Order Details(as PDF)</button></ng-template>\n\t\t</div>\n\t</form>\n</div>\n<ng-template #loadingService><i>Loading ...</i></ng-template>"

/***/ }),

/***/ "../../../../../src/app/services/service-detail/service-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_booking__ = __webpack_require__("../../../../../src/app/models/booking.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_provider__ = __webpack_require__("../../../../../src/app/models/provider.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ServiceDetailComponent = (function () {
    function ServiceDetailComponent(route, data, msg) {
        this.route = route;
        this.data = data;
        this.msg = msg;
        this.providers = [];
        this.requesting = false;
        this.f = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            client: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
                fullname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
                contact: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].email]),
                address: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            }),
            product: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            provider: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
        });
    }
    ServiceDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            var client = { fullname: null, contact: null, address: null };
            _this.order = new __WEBPACK_IMPORTED_MODULE_3__models_booking__["a" /* Booking */]({
                client: JSON.stringify(client),
                client_id: null,
                product: JSON.stringify(data.service),
                provider_id: null,
                provider: null,
                status: 'NEW'
            });
            _this.getProviders(data.service.id);
            _this.title = "'" + _this.order.product.label + "' Service Details";
        });
    };
    ServiceDetailComponent.prototype.book = function () {
        var _this = this;
        console.log(this.f.value);
        if (confirm('Request Service')) {
            var booking_1 = this.f.value;
            this.requesting = true;
            booking_1.product = JSON.stringify(booking_1.product);
            booking_1.client = JSON.stringify(booking_1.client);
            booking_1.provider = JSON.stringify(booking_1.provider);
            booking_1.provider_id = this.order.provider.user_id;
            console.log('booking->', booking_1);
            var msg = ['New Order Request Sent', 'New Order Request Failed'];
            this.data.addRecord('client_order', booking_1, msg).subscribe(function (response) {
                if (response) {
                    _this.msg.addMessage('Your Order was Saved Successfully. Please print the details. Our team will Contact you soon');
                    _this.order_id = response;
                }
                _this.order = new __WEBPACK_IMPORTED_MODULE_3__models_booking__["a" /* Booking */](booking_1);
                _this.requesting = false;
            });
        }
    };
    ServiceDetailComponent.prototype.getProviders = function (id) {
        var _this = this;
        var msg = ['', ''];
        var filter = { service_id: id };
        this.data.getRecords('service_provider', filter, msg).subscribe(function (providers) {
            providers.forEach(function (provider) {
                _this.providers.push(new __WEBPACK_IMPORTED_MODULE_4__models_provider__["a" /* Provider */](provider));
            });
        });
    };
    ServiceDetailComponent.prototype.printOrder = function (id) {
        var _this = this;
        var params = {
            msg: ['Printing Order', 'Printing Order Failed'],
            target: 'orders',
            table: 'client_order',
            filter: { id: id }
        };
        this.data.printRecords(params).subscribe(function (filename) {
            if (filename.length) {
                _this.order_link = filename;
            }
        });
    };
    ServiceDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-service-detail',
            template: __webpack_require__("../../../../../src/app/services/service-detail/service-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/services/service-detail/service-detail.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_5__data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_6__messaging_message_service__["a" /* MessageService */]])
    ], ServiceDetailComponent);
    return ServiceDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/service-list/service-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-service-list{\r\n\tpadding: 5px;\r\n}\r\n\r\n.service-list-header{\r\n\ttext-align: center;\r\n}\r\n\r\n.service-list{\r\n\tpadding: 0;\r\n\tmargin: 0;\r\n\tlist-style-type: none;\r\n}\r\n\r\n.service-item{\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\tmargin: 4px;\r\n\tpadding: 5px;\r\n\tfloat: left;\r\n\theight: 380px;\r\n\twidth: 280px;\r\n}\r\n\r\n.service-item:hover{\r\n\tbackground-color: #dfdfdf;\r\n}\r\n\r\n.service-list-nav{\r\n\tmargin: 0 auto;\r\n\tborder: 1px solid #ccc;\r\n\tborder-radius: 4px;\r\n\theight: 90px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/services/service-list/service-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-service-list\" *ngIf=\"services; else noServices\">\n\t<h2 class=\"service-list-header\">{{title}}</h2>\n\t<nav class=\"service-list-nav\">\n\t\t<input type=\"search\" class=\"input-text\" name=\"search_service\" placeholder=\"Search Services\">\n\t\t<div *ngIf=\"auth.authToken.role=='ADMIN'\">\n\t\t\t<a class=\"link\" target=\"_blank\" *ngIf=\"services_file && !selected_id; else prntServices\" href=\"{{services_file}}\">Download Service(s) Details</a>\n\t\t\t<ng-template #prntServices>\n\t\t\t\t<button class=\"input-btn\"(click)=\"printServices('all')\">Save all Services' Details(as PDF)</button>\n\t\t\t</ng-template>\n\t\t</div>\n\t</nav>\n\t<ul *ngIf=\"services.length; else loadingServices\" class=\"service-list\">\n\t\t<li *ngFor=\"let service of services\" class=\"service-item\">\n\t\t\t<h3>{{service.label}}</h3><hr width=\"90%\" align=\"center\">\n\t\t\t<img class=\"service_img\" src=\"{{service.image}}\" alt=\"{{service.label}}\">\n\t\t\t<p>Cost Kshs.: {{service.service_fee}}</p>\n\t\t\t<a class=\"link\" routerLink=\"/services/{{service.id}}\">See More</a><br/>\n\t\t\t<span *ngIf=\"auth.authToken.l\">\n\t\t\t\t<a class=\"link\" *ngIf=\"auth.authToken.role=='ADMIN'\" routerLink=\"/dashboard/update/{{service.id}}\">\n\t\t\t\t\tEdit Service</a>\n\t\t\t\t<button type=\"button\" class=\"input-btn\" *ngIf=\"auth.authToken.role=='PROVIDER'\"\n\t\t\t\t\t(click)=\"request(service)\">Request to be Provider</button>\n\t\t\t</span>\n\t\t</li>\n\t</ul>\n\t<ng-template #loadingServices><i>No Services to Display</i></ng-template>\n</div>\n<ng-template #noServices><i>Loading Services.. </i></ng-template>"

/***/ }),

/***/ "../../../../../src/app/services/service-list/service-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_service__ = __webpack_require__("../../../../../src/app/models/service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__ = __webpack_require__("../../../../../src/app/messaging/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ServiceListComponent = (function () {
    function ServiceListComponent(data, router, auth, msg) {
        this.data = data;
        this.router = router;
        this.auth = auth;
        this.msg = msg;
        this.services = [];
        this.selected_id = null;
        this.title = "Services";
    }
    ServiceListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getRecords('service', null, ['', 'Fetching Services Failed']).subscribe(function (services) {
            if (services) {
                services.forEach(function (service) {
                    _this.services.push(new __WEBPACK_IMPORTED_MODULE_2__models_service__["a" /* Service */](service));
                });
                console.log(services);
            }
        });
    };
    ServiceListComponent.prototype.request = function (service) {
        if (this.auth.authToken) {
            var _do = false;
            this.auth.authToken.role != 'PROVIDER' ? alert('Please Logout of Admin Account to continue') : _do = true;
            if (_do) {
                this.saveProvider(this.auth.authToken.uid, service);
            }
        }
    };
    ServiceListComponent.prototype.saveProvider = function (id, service) {
        var _this = this;
        var msg = ['Fetching Your Details', 'Fetching User details Failed'];
        this.data.getRecords('user', { id: id }, msg).subscribe(function (users) {
            if (users) {
                var user = { fullname: users[0].fullname, contact: users[0].contact, address: users[0].address, id: users[0].id };
                var provider = {
                    user_id: id,
                    service_id: service.id,
                    provider: JSON.stringify(user),
                    cost: service.service_fee
                };
                console.log('saving provider->', provider);
                msg = ['Saving Request Sent', 'Request Failed'];
                _this.data.addRecord('service_provider', provider, msg).subscribe(function (response) {
                    console.log('save respone->', response);
                    if (response) {
                        _this.msg.addMessage('Your request Approved.');
                    }
                    else {
                        _this.msg.addMessage('Could not Approve your Request at this time. Please Try later.');
                    }
                });
            }
        });
    };
    ServiceListComponent.prototype.printServices = function (param) {
        var _this = this;
        var filter = param === 'all' ? null : { id: param };
        this.selected_id = param === 'all' ? null : param;
        var params = {
            msg: ['Printing Service(s)', 'Printing Service(s) Failed'],
            target: 'services',
            table: 'service',
            filter: filter
        };
        this.data.printRecords(params).subscribe(function (filename) {
            if (filename.length) {
                _this.services_file = filename;
            }
        });
    };
    ServiceListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-service-list',
            template: __webpack_require__("../../../../../src/app/services/service-list/service-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/services/service-list/service-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__messaging_message_service__["a" /* MessageService */]])
    ], ServiceListComponent);
    return ServiceListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/service-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceResolverService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_service__ = __webpack_require__("../../../../../src/app/models/service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ServiceResolverService = (function () {
    function ServiceResolverService(data, router) {
        this.data = data;
        this.router = router;
        this.redirectTo = '/services';
    }
    ServiceResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var id = +route.paramMap.get('service_id');
        console.log(id);
        var filter = { id: id };
        //state.url ? this.redirectTo = state.url : null;
        console.log('state.url->', state.url);
        return this.data.getRecords('service', filter, ['', '']).map(function (services) {
            if (services) {
                var s = new __WEBPACK_IMPORTED_MODULE_5__models_service__["a" /* Service */](services[0]);
                console.log('resolving service', s);
                return s;
            }
            else {
                _this.router.navigate([_this.redirectTo]);
                return null;
            }
        });
    };
    ServiceResolverService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], ServiceResolverService);
    return ServiceResolverService;
}());



/***/ }),

/***/ "../../../../../src/app/services/services.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-services{\r\n\tmargin: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/services/services.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-services\">\n\t<router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/services/services.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServicesComponent = (function () {
    function ServicesComponent() {
    }
    ServicesComponent.prototype.ngOnInit = function () {
    };
    ServicesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-services',
            template: __webpack_require__("../../../../../src/app/services/services.component.html"),
            styles: [__webpack_require__("../../../../../src/app/services/services.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ServicesComponent);
    return ServicesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/services.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_routing__ = __webpack_require__("../../../../../src/app/services/services.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_list_service_list_component__ = __webpack_require__("../../../../../src/app/services/service-list/service-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_detail_service_detail_component__ = __webpack_require__("../../../../../src/app/services/service-detail/service-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_resolver_service__ = __webpack_require__("../../../../../src/app/services/service-resolver.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




//routing





var ServicesModule = (function () {
    function ServicesModule() {
    }
    ServicesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__services_routing__["a" /* ServicesRouting */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__service_list_service_list_component__["a" /* ServiceListComponent */], __WEBPACK_IMPORTED_MODULE_6__service_detail_service_detail_component__["a" /* ServiceDetailComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_7__data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_8__service_resolver_service__["a" /* ServiceResolverService */]]
        })
    ], ServicesModule);
    return ServicesModule;
}());



/***/ }),

/***/ "../../../../../src/app/services/services.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_component__ = __webpack_require__("../../../../../src/app/services/services.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_list_service_list_component__ = __webpack_require__("../../../../../src/app/services/service-list/service-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_detail_service_detail_component__ = __webpack_require__("../../../../../src/app/services/service-detail/service-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_resolver_service__ = __webpack_require__("../../../../../src/app/services/service-resolver.service.ts");





var servicesRoutes = [
    {
        path: 'services', component: __WEBPACK_IMPORTED_MODULE_1__services_component__["a" /* ServicesComponent */],
        children: [
            { path: '', component: __WEBPACK_IMPORTED_MODULE_2__service_list_service_list_component__["a" /* ServiceListComponent */] },
            {
                path: ':service_id', component: __WEBPACK_IMPORTED_MODULE_3__service_detail_service_detail_component__["a" /* ServiceDetailComponent */],
                resolve: { service: __WEBPACK_IMPORTED_MODULE_4__service_resolver_service__["a" /* ServiceResolverService */] }
            },
        ]
    },
];
var ServicesRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forChild(servicesRoutes);


/***/ }),

/***/ "../../../../../src/assets/bg.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "bg.fe39445986c617264254.jpg";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map