<?php
/************************************************************
*	Home Service Application API Endpoint
*	(c) Put your Name here, 2018
*	Author: put your name here
*
*
*
*/
	/*Custom PHP Constants:*/

	// define constant to hold hostname to be used to connect to mysq database
	define('HSA_HOST_NAME', 'localhost');

	// defining constant to hold port to be used to connect to mysq database
	define('HSA_PORT', 3360);

	// define constant to hold username to be used to connect to mysq database
	define('HSA_USERNAME', 'root');

	// define constant to hold database to be used to connect to mysq database
	define('HSA_DB_NAME', 'homeserviceapp_db');

	// define constant to hold password to be used to connect to mysq database
	define('HSA_PASSWORD', '');

	// define constant to hold PHP session name.
	define('HSA_SESSION_NAME', 'hsa_session');
	// hsa_session:- is name of cookie (registered on browser) whose value is the unique ID for the current browser's PHP session

	//load Database class
	require_once 'db.php';

	// load auth functions
	include 'auth.php';

	/* PHP Session Initialization*/
	// a custom function to initialize PHP session
	initSession();

	// logout users
	if(isset($_GET['logout']))
	{
		if(logoutUser())
			header("Location: /");
	}
	// load utils:- backend functions for API
	include 'utils.php';

	/* Begin API logic*/

	// capture POST request data sent from client
	$input = file_get_contents('php://input');

	// convert the request data from JSON (Javascript Object Notation) format to an Object
	$input = json_decode($input);

	// declare an array to hold server details
	$server = [
		'hostname' => HSA_HOST_NAME,  
		'port'=>HSA_PORT,
		'dbname' => HSA_DB_NAME, 
		'username' => HSA_USERNAME, 
		'password' => HSA_PASSWORD
	];

	// instantiate a new database class
	$DB = new DBClass($server);
	
	// when the request has been sent by client try to process request and send back a response
	if(isset($input))
	{
		// declare response to be sent back to client
		$response = ['data'=>null, 'error'=>null];

		// if there were any errors instantiating the database class
		if ($DB->_errors)
		{	
			// then assing them to response error
			$response['error'] = $DB->_errors;
		}
		// otherwise if the database class was instantiated successfully
		elseif ($DB)
		{
			// call a custom request handler function passing it the request data and the database instance
			// then assing the response to what it returns
			$response = RH($input, $DB);
		}
		else
		{	// otherwise assing the response error an uknown error 
			$response['error'] = 'Program/Unknown Error!';
		}

		// call in-built PHP header function to set the header
		header("Content-Type: application/json");

		// print the response in JSON  format
		echo json_encode($response);
	}
?>
